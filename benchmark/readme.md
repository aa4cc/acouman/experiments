# Benchmark

## Description

This utility tests solvers for the "pressure points" optimization problem.
It will test three solvers - LBFGS, Levenberg-Marquardt and Levenberg-Marquardt with constraints on spatial derivatives - on problems with different array sizes, number of points and pressure amplitudes.
The resulting runtimes and qualities of solution are saved to CSV files.

## Compiling and running

The utility requires [Eigen](https://gitlab.com/libeigen/eigen) and [LBFGS++](https://github.com/yixuan/LBFGSpp) libraries to compile.
The provided Makefile is tested on Raspberry Pi 4, running `make test` produces the benchmark utility.

With the current settings - 1000 repetitions of each algorithm, 9 array configurations, 1 to 4 points, 7 different amplitudes - the benchmark takes approximately 6 hours to complete.


