//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  LM_solve_gradient_rtwutil.cpp
//
//  Code generation for function 'LM_solve_gradient_rtwutil'
//


// Include files
#include "rtwutil.h"
#include "LM_solve_gradient.h"
#include "rt_nonfinite.h"
#include <cmath>

// Function Definitions
double rt_roundd_snf(double u)
{
  double y;
  if (std::abs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = std::floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = std::ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

// End of code generation (LM_solve_gradient_rtwutil.cpp)
