//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  generate_lookup_data.cpp
//
//  Code generation for function 'generate_lookup_data'
//


// Include files
#include "generate_lookup_data.h"
#include "rt_nonfinite.h"

// Variable Definitions
double sinAngle[1001];
double directivity[1001];
double directivity_derivative[1001];
bool isInitialized_lookup = false;

// End of code generation (generate_lookup_data.cpp)
