//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure.cpp
//
//  Code generation for function 'absolute_pressure'
//


// Include files
#include "absolute_pressure.h"
#include "absolute_pressure_wrapper.h"
#include "rt_nonfinite.h"
#include <cmath>

// Function Declarations
static double rt_roundd_snf(double u);

// Function Definitions
static double rt_roundd_snf(double u)
{
  double y;
  if (std::abs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = std::floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = std::ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

void absolute_pressure(const double M_data[], const int M_size[2], const double
  c_data[], const int c_size[1], const double s_data[], const int s_size[1],
  double abs_p_data[], int abs_p_size[1])
{
  int num_pp;
  int varargin_1_size_idx_0;
  int loop_ub;
  int b_loop_ub;
  int c_loop_ub;
  double y_idx_0;
  double y_idx_1;
  double b_y_idx_0;
  double Mi_data[512];
  double varargin_1_data[256];
  double s;
  double y_data[256];
  double b_y_data[256];
  num_pp = static_cast<int>(static_cast<unsigned int>(rt_roundd_snf(static_cast<
    double>(M_size[1]) / 2.0)));
  abs_p_size[0] = num_pp;
  if (0 <= num_pp - 1) {
    varargin_1_size_idx_0 = M_size[0];
    loop_ub = c_size[0];
    b_loop_ub = s_size[0];
    c_loop_ub = c_size[0];
  }

  for (int i = 0; i < num_pp; i++) {
    int b_i;
    int k;
    b_i = static_cast<int>(((i + 1U) << 1));

    //          P1 = Mi*Mi';
    //          P2 = Mi*[0,-2;2,0]*Mi';
    //
    //          abs_p(i) = c'*P1*c + s'*P1*s + c'*P2*s;
    y_idx_0 = 0.0;
    y_idx_1 = 0.0;
    for (k = 0; k < varargin_1_size_idx_0; k++) {
      b_y_idx_0 = M_data[k + M_size[0] * (b_i - 2)];
      Mi_data[k] = b_y_idx_0;
      s = M_data[k + M_size[0] * (b_i - 1)];
      Mi_data[k + varargin_1_size_idx_0] = s;
      y_idx_0 += b_y_idx_0 * c_data[k];
      y_idx_1 += s * c_data[k];
    }

    for (b_i = 0; b_i < varargin_1_size_idx_0; b_i++) {
      varargin_1_data[b_i] = Mi_data[b_i] * y_idx_0 +
        Mi_data[varargin_1_size_idx_0 + b_i] * y_idx_1;
    }

    y_idx_0 = 0.0;
    y_idx_1 = 0.0;
    for (k = 0; k < varargin_1_size_idx_0; k++) {
      y_idx_0 += Mi_data[k] * s_data[k];
      y_idx_1 += Mi_data[varargin_1_size_idx_0 + k] * s_data[k];
    }

    for (b_i = 0; b_i < varargin_1_size_idx_0; b_i++) {
      y_data[b_i] = Mi_data[b_i] * y_idx_0 + Mi_data[varargin_1_size_idx_0 + b_i]
        * y_idx_1;
    }

    y_idx_0 = 0.0;
    y_idx_1 = 0.0;
    for (k = 0; k < varargin_1_size_idx_0; k++) {
      y_idx_0 += Mi_data[k] * s_data[k];
      y_idx_1 += Mi_data[varargin_1_size_idx_0 + k] * s_data[k];
    }

    b_y_idx_0 = 0.0 * y_idx_0 + -2.0 * y_idx_1;
    y_idx_1 = 2.0 * y_idx_0 + 0.0 * y_idx_1;
    for (b_i = 0; b_i < varargin_1_size_idx_0; b_i++) {
      b_y_data[b_i] = Mi_data[b_i] * b_y_idx_0 + Mi_data[varargin_1_size_idx_0 +
        b_i] * y_idx_1;
    }

    b_y_idx_0 = 0.0;
    for (b_i = 0; b_i < loop_ub; b_i++) {
      b_y_idx_0 += c_data[b_i] * varargin_1_data[b_i];
    }

    s = 0.0;
    for (b_i = 0; b_i < b_loop_ub; b_i++) {
      s += s_data[b_i] * y_data[b_i];
    }

    y_idx_0 = 0.0;
    for (b_i = 0; b_i < c_loop_ub; b_i++) {
      y_idx_0 += c_data[b_i] * b_y_data[b_i];
    }

    abs_p_data[i] = (b_y_idx_0 + s) + y_idx_0;
  }
}

// End of code generation (absolute_pressure.cpp)
