#include "utils.h"
#include <iostream>

void generate_array(int array_size, double *tx, double *ty) {
    double x = - 0.005 * (array_size - 1);
    double y = x;

    int i,j;
    for (i = 0; i < array_size; i++) {
        for (j = 0; j < array_size; j++) {
            tx[array_size*i + j] = x;
            ty[array_size*i + j] = y;
            y += 0.01;
        }
        y = - 0.005 * (array_size - 1);
        x += 0.01;
    }    
}

void print_vector(double *x, int len, const char* sep) {
    int i;
    for (i = 0; i < len - 1; i++) {
        std::cout << x[i] << sep;
    }
    std::cout << x[len - 1];
}
