//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  terminate.cpp
//
//  Code generation for function 'terminate'
//


// Include files
#include "terminate.h"
#include "absolute_pressure_derivative_wrapper.h"
#include "rt_nonfinite.h"

// Function Definitions
void lookup_terminate()
{
  // (no terminate code required)
  isInitialized_lookup = false;
}

// End of code generation (terminate.cpp)
