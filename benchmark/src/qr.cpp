//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  qr.cpp
//
//  Code generation for function 'qr'
//


// Include files
#include "qr.h"
#include "LM_solve_gradient.h"
#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <cstring>

// Function Definitions
void qr(double A_data[], const int A_size[2])
{
  int u0;
  int u1;
  double tau_data[20];
  u0 = A_size[0];
  u1 = A_size[1];
  if (u0 < u1) {
    u1 = u0;
  }

  if ((A_size[0] != 0) && (A_size[1] != 0) && (u1 >= 1)) {
    std::memset(&tau_data[0], 0, u1 * sizeof(double));
    qrf(A_data, A_size, A_size[0], A_size[1], u1, tau_data);
  }
}

void qr(coder::array<double, 2U> &A)
{
  int u0;
  int minmana;
  int minmn;
  double tau_data[30];
  u0 = A.size(0);
  minmana = A.size(1);
  if (u0 < minmana) {
    minmana = u0;
  }

  u0 = A.size(0);
  minmn = A.size(1);
  if (u0 < minmn) {
    minmn = u0;
  }

  if ((A.size(0) != 0) && (A.size(1) != 0) && (minmn >= 1)) {
    if (0 <= minmana - 1) {
      std::memset(&tau_data[0], 0, minmana * sizeof(double));
    }

    qrf(A, A.size(0), A.size(1), minmn, tau_data);
  }
}

// End of code generation (qr.cpp)
