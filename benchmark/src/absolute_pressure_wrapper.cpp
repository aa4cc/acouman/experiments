//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_wrapper.cpp
//
//  Code generation for function 'absolute_pressure_wrapper'
//


// Include files
#include "absolute_pressure_wrapper.h"
#include "absolute_pressure.h"
#include "generate_lookup_data.h"
#include "generate_lookup.h"
#include "rt_nonfinite.h"
#include <cmath>
#include <cstring>

// Function Definitions
void absolute_pressure_wrapper(const double x_data[], const int x_size[1], const
  double y_data[], const int [1], const double z_data[], const int [1], const
  double tx_data[], const int tx_size[1], const double ty_data[], const int [1],
  const double phases_data[], const int phases_size[1], double P_data[], int
  P_size[1])
{
  int M_size[2];
  int low_i;
  double M_data[5120];
  int i;
  int c_size[1];
  double c_data[256];
  int low_ip1;
  int s_size[1];
  double s_data[256];
  double y[1001];
  double x[1001];
  double Vq;
  if (!isInitialized_lookup) {
    generate_lookup();
  }

  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  M_size[0] = tx_size[0];
  low_i = x_size[0] << 1;
  M_size[1] = low_i;
  low_i *= tx_size[0];
  if (0 <= low_i - 1) {
    std::memset(&M_data[0], 0, low_i * sizeof(double));
  }

  i = x_size[0];
  for (int b_i = 0; b_i < i; b_i++) {
    int i1;

    //  iterate over points
    i1 = tx_size[0];
    for (int j = 0; j < i1; j++) {
      double xr;
      double yr;
      double d;

      //  iterate over transducers
      xr = x_data[b_i] - tx_data[j];
      yr = y_data[b_i] - ty_data[j];
      xr = xr * xr + yr * yr;
      d = std::sqrt(xr + z_data[b_i] * z_data[b_i]);

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      yr = std::sqrt(xr) / d;
      std::memcpy(&y[0], &directivity[0], 1001U * sizeof(double));
      std::memcpy(&x[0], &sinAngle[0], 1001U * sizeof(double));
      low_ip1 = 0;
      int exitg1;
      do {
        exitg1 = 0;
        if (low_ip1 < 1001) {
          if (rtIsNaN(sinAngle[low_ip1])) {
            exitg1 = 1;
          } else {
            low_ip1++;
          }
        } else {
          if (sinAngle[1] < sinAngle[0]) {
            for (low_i = 0; low_i < 500; low_i++) {
              xr = x[low_i];
              x[low_i] = x[1000 - low_i];
              x[1000 - low_i] = xr;
              xr = y[low_i];
              y[low_i] = y[1000 - low_i];
              y[1000 - low_i] = xr;
            }
          }

          Vq = rtNaN;
          if ((!rtIsNaN(yr)) && (!(yr > x[1000])) && (!(yr < x[0]))) {
            int high_i;
            low_i = 1;
            low_ip1 = 2;
            high_i = 1001;
            while (high_i > low_ip1) {
              int mid_i;
              mid_i = (low_i + high_i) >> 1;
              if (yr >= x[mid_i - 1]) {
                low_i = mid_i;
                low_ip1 = mid_i + 1;
              } else {
                high_i = mid_i;
              }
            }

            xr = x[low_i - 1];
            xr = (yr - xr) / (x[low_i] - xr);
            if (xr == 0.0) {
              Vq = y[low_i - 1];
            } else if (xr == 1.0) {
              Vq = y[low_i];
            } else if (y[low_i - 1] == y[low_i]) {
              Vq = y[low_i - 1];
            } else {
              Vq = (1.0 - xr) * y[low_i - 1] + xr * y[low_i];
            }
          }

          exitg1 = 1;
        }
      } while (exitg1 == 0);

      yr = 5.0 * Vq / d;
      xr = 732.733 * d;
      low_i = (b_i + 1) << 1;
      M_data[j + M_size[0] * (low_i - 2)] = yr * std::cos(xr);
      M_data[j + M_size[0] * (low_i - 1)] = yr * std::sin(xr);
    }
  }

  c_size[0] = phases_size[0];
  low_i = phases_size[0];
  if (0 <= low_i - 1) {
    std::memcpy(&c_data[0], &phases_data[0], low_i * sizeof(double));
  }

  low_i = phases_size[0];
  for (low_ip1 = 0; low_ip1 < low_i; low_ip1++) {
    c_data[low_ip1] = std::cos(c_data[low_ip1]);
  }

  s_size[0] = phases_size[0];
  low_i = phases_size[0];
  if (0 <= low_i - 1) {
    std::memcpy(&s_data[0], &phases_data[0], low_i * sizeof(double));
  }

  low_i = phases_size[0];
  for (low_ip1 = 0; low_ip1 < low_i; low_ip1++) {
    s_data[low_ip1] = std::sin(s_data[low_ip1]);
  }

  absolute_pressure(M_data, M_size, c_data, c_size, s_data, s_size, P_data,
                    P_size);
}

// End of code generation (absolute_pressure_wrapper.cpp)
