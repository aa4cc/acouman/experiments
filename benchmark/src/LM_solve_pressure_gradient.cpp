//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  LM_solve_pressure_gradient.cpp
//
//  Code generation for function 'LM_solve_pressure_gradient'
//


// Include files
#include "LM_solve_pressure_gradient.h"
#include "coder_array.h"
#include "generate_lookup_data.h"
#include "generate_lookup.h"
#include "absolute_pressure.h"
#include "absolute_pressure_derivative.h"
#include "interp1.h"
#include "pressure_derivative_jacobian.h"
#include "pressure_jacobian.h"
#include "qr.h"
#include "rt_nonfinite.h"
#include "trisolve.h"
#include <cmath>
#include <cstring>
#include <math.h>

// Function Declarations
static double rt_powd_snf(double u0, double u1);

// Function Definitions
static double rt_powd_snf(double u0, double u1)
{
  double y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    double d;
    double d1;
    d = std::abs(u0);
    d1 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (d == 1.0) {
        y = 1.0;
      } else if (d > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void LM_solve_pressure_gradient(const double x_data[], const int x_size[1],
  const double y_data[], const int [1], const double z_data[], const int [1],
  const double reqP_data[], const int reqP_size[1], const double tx_data[],
  const int tx_size[1], const double ty_data[], const int [1], double
  phases_data[], int phases_size[1])
{
  int nx;
  int M_size[2];
  int num_search;
  static double M_data[5120];
  int i;
  int b_i;
  int k;
  int minszA;
  int Mx_size[2];
  double xr;
  double Mx_data[5120];
  double yr;
  int My_size[2];
  double p;
  double d;
  double My_data[5120];
  double phase_shift;
  int M_tmp;
  int c_size[1];
  double c_data[256];
  int s_size[1];
  double s_data[256];
  double P_data[10];
  int P_size[1];
  double Px_data[10];
  int Px_size[1];
  double Py_data[10];
  int Py_size[1];
  double penalty;
  int o_size_idx_0;
  double o_data[30];
  int obj_fit_size_idx_0;
  double obj_fit_data[30];
  double Jp_data[2560];
  int Jp_size[2];
  double Jx_data[2560];
  int Jx_size[2];
  double Jy_data[2560];
  int Jy_size[2];
  int u0;
  bool empty_non_axis_sizes;
  signed char input_sizes_idx_0;
  signed char b_input_sizes_idx_0;
  signed char sizes_idx_0;
  int J_size_idx_0;
  static double J_data[7680];
  int nump_pp;
  bool exitg1;
  double rhs_data[30];
  double I_data[900];
  static double varargin_1_tmp_data[7680];
  coder::array<double, 2U> R;
  coder::array<double, 2U> b_R;
  int I_size[2];
  int new_o_size[1];
  double new_o_data[30];
  int C_size[1];
  double C_data[30];
  int new_s_size[1];
  double new_s_data[256];
  if (!isInitialized_lookup) {
    generate_lookup();
  }

  //     %% Preparation
  //      phases = pi*randn(array_size^2,1); % initial guess
  phases_size[0] = tx_size[0];
  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&phases_data[0], 0, nx * sizeof(double));
  }

  //  initial guess
  //  calculate acoustic pressure
  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  M_size[0] = tx_size[0];
  num_search = x_size[0] << 1;
  M_size[1] = num_search;
  nx = tx_size[0] * num_search;
  if (0 <= nx - 1) {
    std::memset(&M_data[0], 0, nx * sizeof(double));
  }

  i = x_size[0];
  for (b_i = 0; b_i < i; b_i++) {
    //  iterate over points
    k = tx_size[0];
    for (minszA = 0; minszA < k; minszA++) {
      //  iterate over transducers
      xr = x_data[b_i] - tx_data[minszA];
      yr = y_data[b_i] - ty_data[minszA];
      p = xr * xr + yr * yr;
      d = std::sqrt(p + z_data[b_i] * z_data[b_i]);

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      p = 5.0 * interp1(sinAngle, directivity, std::sqrt(p) / d) / d;
      phase_shift = 732.733 * d;
      M_tmp = (b_i + 1) << 1;
      M_data[minszA + M_size[0] * (M_tmp - 2)] = p * std::cos(phase_shift);
      M_data[minszA + M_size[0] * (M_tmp - 1)] = p * std::sin(phase_shift);
    }
  }

  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  Mx_size[0] = tx_size[0];
  Mx_size[1] = num_search;
  if (0 <= nx - 1) {
    std::memset(&Mx_data[0], 0, nx * sizeof(double));
  }

  My_size[0] = tx_size[0];
  My_size[1] = num_search;
  if (0 <= nx - 1) {
    std::memset(&My_data[0], 0, nx * sizeof(double));
  }

  i = x_size[0];
  for (b_i = 0; b_i < i; b_i++) {
    //  iterate over points
    k = tx_size[0];
    for (minszA = 0; minszA < k; minszA++) {
      double d_tmp;
      double f_dir;
      double pxr;
      double pxi_tmp;
      double b_pxi_tmp;
      double pxi;

      //  iterate over transducers
      xr = x_data[b_i] - tx_data[minszA];
      yr = y_data[b_i] - ty_data[minszA];
      p = xr * xr + yr * yr;
      d_tmp = z_data[b_i] * z_data[b_i];
      d = std::sqrt(p + d_tmp);
      p = std::sqrt(p) / d;

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      f_dir = interp1(sinAngle, directivity, p);
      p = interp1(sinAngle, directivity_derivative, p);
      penalty = rt_powd_snf(d, 5.0);
      phase_shift = rt_powd_snf(d, 3.0);
      pxr = 5.0 * (p * xr * d_tmp / penalty - f_dir * xr / phase_shift);
      pxi_tmp = 5.0 * f_dir * 732.733;
      b_pxi_tmp = d * d;
      pxi = pxi_tmp * xr / b_pxi_tmp;
      f_dir = 5.0 * (p * yr * d_tmp / penalty - f_dir * yr / phase_shift);
      xr = pxi_tmp * yr / b_pxi_tmp;
      phase_shift = 732.733 * d;
      penalty = std::sin(phase_shift);
      p = std::cos(phase_shift);
      nx = (b_i + 1) << 1;
      M_tmp = nx - 2;
      Mx_data[minszA + Mx_size[0] * M_tmp] = pxr * p - pxi * penalty;
      nx--;
      Mx_data[minszA + Mx_size[0] * nx] = pxr * penalty + pxi * p;
      My_data[minszA + My_size[0] * M_tmp] = f_dir * p - xr * penalty;
      My_data[minszA + My_size[0] * nx] = f_dir * penalty + xr * p;
    }
  }

  c_size[0] = tx_size[0];
  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&c_data[0], 0, nx * sizeof(double));
  }

  nx = tx_size[0];
  for (k = 0; k < nx; k++) {
    c_data[k] = std::cos(c_data[k]);
  }

  s_size[0] = tx_size[0];
  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&s_data[0], 0, nx * sizeof(double));
  }

  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&s_data[0], 0, nx * sizeof(double));
  }

  absolute_pressure(M_data, M_size, c_data, c_size, s_data, s_size, P_data,
                    P_size);
  absolute_pressure_derivative(M_data, M_size, Mx_data, Mx_size, My_data,
    My_size, c_data, c_size, s_data, s_size, Px_data, Px_size, Py_data, Py_size);
  o_size_idx_0 = (P_size[0] + Px_size[0]) + Py_size[0];
  nx = P_size[0];
  if (0 <= nx - 1) {
    std::memcpy(&o_data[0], &P_data[0], nx * sizeof(double));
  }

  nx = Px_size[0];
  for (i = 0; i < nx; i++) {
    o_data[i + P_size[0]] = Px_data[i];
  }

  nx = Py_size[0];
  for (i = 0; i < nx; i++) {
    o_data[(i + P_size[0]) + Px_size[0]] = Py_data[i];
  }

  nx = reqP_size[0];
  for (k = 0; k < nx; k++) {
    P_data[k] = reqP_data[k] * reqP_data[k];
  }

  obj_fit_size_idx_0 = reqP_size[0] + num_search;
  nx = reqP_size[0];
  if (0 <= nx - 1) {
    std::memcpy(&obj_fit_data[0], &P_data[0], nx * sizeof(double));
  }

  if (0 <= num_search - 1) {
    std::memset(&obj_fit_data[reqP_size[0]], 0, num_search * sizeof(double));
  }

  penalty = 0.0;
  for (i = 0; i < obj_fit_size_idx_0; i++) {
    p = obj_fit_data[i] - o_data[i];
    penalty += p * p;
  }

  pressure_jacobian(M_data, M_size, c_data, c_size, s_data, s_size, Jp_data,
                    Jp_size);
  pressure_derivative_jacobian(M_data, M_size, Mx_data, Mx_size, My_data,
    My_size, c_data, s_data, Jx_data, Jx_size, Jy_data, Jy_size);
  if ((Jp_size[0] != 0) && (Jp_size[1] != 0)) {
    u0 = Jp_size[1];
  } else if ((Jx_size[0] != 0) && (Jx_size[1] != 0)) {
    u0 = Jx_size[1];
  } else if ((Jy_size[0] != 0) && (Jy_size[1] != 0)) {
    u0 = Jy_size[1];
  } else {
    u0 = Jp_size[1];
    if (u0 <= 0) {
      u0 = 0;
    }

    if (Jx_size[1] > u0) {
      u0 = Jx_size[1];
    }

    if (Jy_size[1] > u0) {
      u0 = Jy_size[1];
    }
  }

  empty_non_axis_sizes = (u0 == 0);
  if (empty_non_axis_sizes || ((Jp_size[0] != 0) && (Jp_size[1] != 0))) {
    input_sizes_idx_0 = static_cast<signed char>(Jp_size[0]);
  } else {
    input_sizes_idx_0 = 0;
  }

  if (empty_non_axis_sizes || ((Jx_size[0] != 0) && (Jx_size[1] != 0))) {
    b_input_sizes_idx_0 = static_cast<signed char>(Jx_size[0]);
  } else {
    b_input_sizes_idx_0 = 0;
  }

  if (empty_non_axis_sizes || ((Jy_size[0] != 0) && (Jy_size[1] != 0))) {
    sizes_idx_0 = static_cast<signed char>(Jy_size[0]);
  } else {
    sizes_idx_0 = 0;
  }

  nx = input_sizes_idx_0;
  minszA = b_input_sizes_idx_0;
  M_tmp = sizes_idx_0;
  J_size_idx_0 = (input_sizes_idx_0 + b_input_sizes_idx_0) + sizes_idx_0;
  for (i = 0; i < u0; i++) {
    for (k = 0; k < nx; k++) {
      J_data[k + J_size_idx_0 * i] = Jp_data[k + input_sizes_idx_0 * i];
    }
  }

  for (i = 0; i < u0; i++) {
    for (k = 0; k < minszA; k++) {
      J_data[(k + input_sizes_idx_0) + J_size_idx_0 * i] = Jx_data[k +
        b_input_sizes_idx_0 * i];
    }
  }

  for (i = 0; i < u0; i++) {
    for (k = 0; k < M_tmp; k++) {
      J_data[((k + input_sizes_idx_0) + b_input_sizes_idx_0) + J_size_idx_0 * i]
        = Jy_data[k + sizes_idx_0 * i];
    }
  }

  nump_pp = 3 * x_size[0];

  //     %% Levenberg-Marquardt solver
  xr = 1.0;

  //  initial damping
  //      fprintf('|Iteration|Penalty |Damping |LSCount|\n');
  //      fprintf('|---------|--------|--------|-------|\n');
  b_i = 0;
  exitg1 = false;
  while ((!exitg1) && (b_i < 7)) {
    //  number of iterations
    for (i = 0; i < o_size_idx_0; i++) {
      rhs_data[i] = o_data[i] - obj_fit_data[i];
    }

    num_search = 1;
    int exitg2;
    do {
      short c_input_sizes_idx_0;
      exitg2 = 0;
      p = std::sqrt(xr);
      nx = nump_pp * nump_pp;
      if (0 <= nx - 1) {
        std::memset(&I_data[0], 0, nx * sizeof(double));
      }

      if (nump_pp > 0) {
        for (k = 0; k < nump_pp; k++) {
          I_data[k + nump_pp * k] = 1.0;
        }
      }

      nx = nump_pp * nump_pp;
      for (i = 0; i < nx; i++) {
        I_data[i] *= p;
      }

      for (i = 0; i < J_size_idx_0; i++) {
        for (k = 0; k < u0; k++) {
          varargin_1_tmp_data[k + u0 * i] = J_data[i + J_size_idx_0 * k];
        }
      }

      if ((u0 != 0) && (J_size_idx_0 != 0)) {
        M_tmp = J_size_idx_0;
      } else if (nump_pp != 0) {
        M_tmp = nump_pp;
      } else if (J_size_idx_0 > 0) {
        M_tmp = J_size_idx_0;
      } else {
        M_tmp = 0;
      }

      empty_non_axis_sizes = (M_tmp == 0);
      if (empty_non_axis_sizes || ((u0 != 0) && (J_size_idx_0 != 0))) {
        c_input_sizes_idx_0 = static_cast<short>(u0);
      } else {
        c_input_sizes_idx_0 = 0;
      }

      if (empty_non_axis_sizes || (nump_pp != 0)) {
        input_sizes_idx_0 = static_cast<signed char>(nump_pp);
      } else {
        input_sizes_idx_0 = 0;
      }

      //              [~, R] = qr(Abar, 0);
      nx = c_input_sizes_idx_0;
      minszA = input_sizes_idx_0;
      R.set_size((c_input_sizes_idx_0 + input_sizes_idx_0), M_tmp);
      for (i = 0; i < M_tmp; i++) {
        for (k = 0; k < nx; k++) {
          R[k + R.size(0) * i] = varargin_1_tmp_data[k + c_input_sizes_idx_0 * i];
        }
      }

      for (i = 0; i < M_tmp; i++) {
        for (k = 0; k < minszA; k++) {
          R[(k + c_input_sizes_idx_0) + R.size(0) * i] = I_data[k +
            input_sizes_idx_0 * i];
        }
      }

      qr(R);
      if (1 > nump_pp) {
        nx = 0;
        M_tmp = 0;
      } else {
        nx = nump_pp;
        M_tmp = nump_pp;
      }

      b_R.set_size(nx, M_tmp);
      for (i = 0; i < M_tmp; i++) {
        for (k = 0; k < nx; k++) {
          b_R[k + b_R.size(0) * i] = R[k + R.size(0) * i];
        }
      }

      R.set_size(b_R.size(0), b_R.size(1));
      nx = b_R.size(0) * b_R.size(1);
      for (i = 0; i < nx; i++) {
        R[i] = b_R[i];
      }

      //              step = -J'*((R'*R)\rhs);
      b_R.set_size(R.size(1), R.size(0));
      nx = R.size(0);
      for (i = 0; i < nx; i++) {
        M_tmp = R.size(1);
        for (k = 0; k < M_tmp; k++) {
          b_R[k + b_R.size(0) * i] = R[i + R.size(0) * k];
        }
      }

      I_size[0] = R.size(1);
      I_size[1] = R.size(0);
      nx = R.size(1) * R.size(0);
      for (i = 0; i < nx; i++) {
        I_data[i] = b_R[i];
      }

      nx = I_size[1];
      M_tmp = I_size[0];
      minszA = I_size[1];
      if (M_tmp < minszA) {
        minszA = M_tmp;
      }

      new_o_size[0] = I_size[1];
      if (0 <= minszA - 1) {
        std::memcpy(&new_o_data[0], &rhs_data[0], minszA * sizeof(double));
      }

      i = minszA + 1;
      if (i <= nx) {
        std::memset(&new_o_data[i + -1], 0, ((nx - i) + 1) * sizeof(double));
      }

      trisolve(I_data, I_size, new_o_data, new_o_size);
      nx = R.size(1);
      M_tmp = R.size(0);
      minszA = R.size(1);
      if (M_tmp < minszA) {
        minszA = M_tmp;
      }

      C_size[0] = R.size(1);
      if (0 <= minszA - 1) {
        std::memcpy(&C_data[0], &new_o_data[0], minszA * sizeof(double));
      }

      i = minszA + 1;
      if (i <= nx) {
        std::memset(&C_data[i + -1], 0, ((nx - i) + 1) * sizeof(double));
      }

      b_trisolve(R.data(), R.size_ptr(), C_data, C_size);
      nx = u0 * J_size_idx_0;
      for (i = 0; i < nx; i++) {
        varargin_1_tmp_data[i] = -varargin_1_tmp_data[i];
      }

      M_tmp = u0 - 1;
      if (0 <= M_tmp) {
        std::memset(&c_data[0], 0, (M_tmp + 1) * sizeof(double));
      }

      for (k = 0; k < J_size_idx_0; k++) {
        nx = k * u0;
        for (minszA = 0; minszA <= M_tmp; minszA++) {
          c_data[minszA] += varargin_1_tmp_data[nx + minszA] * C_data[k];
        }
      }

      c_size[0] = phases_size[0];
      nx = phases_size[0];
      for (i = 0; i < nx; i++) {
        c_data[i] += phases_data[i];
      }

      s_size[0] = phases_size[0];
      nx = phases_size[0];
      if (0 <= nx - 1) {
        std::memcpy(&s_data[0], &c_data[0], nx * sizeof(double));
      }

      nx = phases_size[0];
      for (k = 0; k < nx; k++) {
        s_data[k] = std::cos(s_data[k]);
      }

      new_s_size[0] = phases_size[0];
      nx = phases_size[0];
      if (0 <= nx - 1) {
        std::memcpy(&new_s_data[0], &c_data[0], nx * sizeof(double));
      }

      nx = phases_size[0];
      for (k = 0; k < nx; k++) {
        new_s_data[k] = std::sin(new_s_data[k]);
      }

      absolute_pressure(M_data, M_size, s_data, s_size, new_s_data, new_s_size,
                        P_data, P_size);
      absolute_pressure_derivative(M_data, M_size, Mx_data, Mx_size, My_data,
        My_size, s_data, s_size, new_s_data, new_s_size, Px_data, Px_size,
        Py_data, Py_size);
      new_o_size[0] = (P_size[0] + Px_size[0]) + Py_size[0];
      nx = P_size[0];
      if (0 <= nx - 1) {
        std::memcpy(&new_o_data[0], &P_data[0], nx * sizeof(double));
      }

      nx = Px_size[0];
      for (i = 0; i < nx; i++) {
        new_o_data[i + P_size[0]] = Px_data[i];
      }

      nx = Py_size[0];
      for (i = 0; i < nx; i++) {
        new_o_data[(i + P_size[0]) + Px_size[0]] = Py_data[i];
      }

      phase_shift = 0.0;
      for (i = 0; i < obj_fit_size_idx_0; i++) {
        p = obj_fit_data[i] - new_o_data[i];
        phase_shift += p * p;
      }

      if (phase_shift < penalty) {
        nx = c_size[0];
        if (0 <= nx - 1) {
          std::memcpy(&phases_data[0], &c_data[0], nx * sizeof(double));
        }

        o_size_idx_0 = new_o_size[0];
        nx = new_o_size[0];
        if (0 <= nx - 1) {
          std::memcpy(&o_data[0], &new_o_data[0], nx * sizeof(double));
        }

        penalty = phase_shift;

        //                  fprintf('|%9d|%8.2e|%8.2e|%7d|\n',i,penalty,mu,num_search); 
        pressure_jacobian(M_data, M_size, s_data, s_size, new_s_data, new_s_size,
                          Jp_data, Jp_size);
        pressure_derivative_jacobian(M_data, M_size, Mx_data, Mx_size, My_data,
          My_size, s_data, new_s_data, Jx_data, Jx_size, Jy_data, Jy_size);
        if ((Jp_size[0] != 0) && (Jp_size[1] != 0)) {
          u0 = Jp_size[1];
        } else if ((Jx_size[0] != 0) && (Jx_size[1] != 0)) {
          u0 = Jx_size[1];
        } else if ((Jy_size[0] != 0) && (Jy_size[1] != 0)) {
          u0 = Jy_size[1];
        } else {
          u0 = Jp_size[1];
          if (u0 <= 0) {
            u0 = 0;
          }

          if (Jx_size[1] > u0) {
            u0 = Jx_size[1];
          }

          if (Jy_size[1] > u0) {
            u0 = Jy_size[1];
          }
        }

        empty_non_axis_sizes = (u0 == 0);
        if (empty_non_axis_sizes || ((Jp_size[0] != 0) && (Jp_size[1] != 0))) {
          input_sizes_idx_0 = static_cast<signed char>(Jp_size[0]);
        } else {
          input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((Jx_size[0] != 0) && (Jx_size[1] != 0))) {
          b_input_sizes_idx_0 = static_cast<signed char>(Jx_size[0]);
        } else {
          b_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((Jy_size[0] != 0) && (Jy_size[1] != 0))) {
          sizes_idx_0 = static_cast<signed char>(Jy_size[0]);
        } else {
          sizes_idx_0 = 0;
        }

        nx = input_sizes_idx_0;
        minszA = b_input_sizes_idx_0;
        M_tmp = sizes_idx_0;
        J_size_idx_0 = (input_sizes_idx_0 + b_input_sizes_idx_0) + sizes_idx_0;
        for (i = 0; i < u0; i++) {
          for (k = 0; k < nx; k++) {
            J_data[k + J_size_idx_0 * i] = Jp_data[k + input_sizes_idx_0 * i];
          }
        }

        for (i = 0; i < u0; i++) {
          for (k = 0; k < minszA; k++) {
            J_data[(k + input_sizes_idx_0) + J_size_idx_0 * i] = Jx_data[k +
              b_input_sizes_idx_0 * i];
          }
        }

        for (i = 0; i < u0; i++) {
          for (k = 0; k < M_tmp; k++) {
            J_data[((k + input_sizes_idx_0) + b_input_sizes_idx_0) +
              J_size_idx_0 * i] = Jy_data[k + sizes_idx_0 * i];
          }
        }

        xr /= 2.0;
        exitg2 = 1;
      } else {
        xr *= 2.0;
        num_search = static_cast<int>((num_search + 1U));
        if (num_search > 100) {
          //                 fprintf('LMsolve failed to converge on iteration %u\n',i); 
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    if (penalty < 1.0) {
      //  penalty tolerance
      exitg1 = true;
    } else {
      b_i++;
    }
  }
}

// End of code generation (LM_solve_pressure_gradient.cpp)
