#include "absolute_pressure_derivative_wrapper.h"
#include "absolute_pressure_wrapper.h"
//#include "LM_solve_gradient.h"
#include "LM_solve_pressure.h"
#include "LM_solve_pressure_gradient.h"
#include "pressurePoints.h"
#include "generate_lookup.h"
#include "terminate.h"
#include "utils.h"

#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>

#define MIN_ARRAY_SIZE  8
#define MAX_ARRAY_SIZE  16
#define N_ARRAY_SETUPS  (MAX_ARRAY_SIZE - MIN_ARRAY_SIZE + 1)
#define N_POINT_SETUPS  4
#define MAX_POINTS      4
#define N_AMPLITUDES    7
#define N_REPEATS       1000
#define STRING_BUFFER   30

using namespace std;

void fill_vector(double *x, int size, double value) {
    for (int i = 0; i < size; i++) {
        x[i] = value;
    }
}

double squared_error(double *P_req, double *P, int size) {
    double result = 0;
    for (int i = 0; i < size; i++) {
        double diff = P_req[i] - std::sqrt(P[i]);
        result += diff*diff;
    }
    return result;
}

void save_to_csv(char *filename, double values[N_POINT_SETUPS][N_AMPLITUDES], const char row_names[N_POINT_SETUPS][STRING_BUFFER], const char column_names[N_AMPLITUDES][STRING_BUFFER]) {
    ofstream outfile;
    outfile.open(filename, ios::out | ios::trunc );
    outfile << "Amplitudes";
    for (int i = 0; i < N_AMPLITUDES; i++) {
        outfile << ", " << column_names[i];
    }
    outfile << endl;
    for (int i = 0; i < N_POINT_SETUPS; i++) {
        outfile << row_names[i];
        for (int j = 0; j < N_AMPLITUDES; j++)
            outfile << ", " << values[i][j];
        outfile << endl;
    }
    outfile.close();
}

int main() {
    // Variables for storing array parameters
    double tx[MAX_ARRAY_SIZE*MAX_ARRAY_SIZE];
    double ty[MAX_ARRAY_SIZE*MAX_ARRAY_SIZE];
    double phases[MAX_ARRAY_SIZE*MAX_ARRAY_SIZE];
    double phases_initial[MAX_ARRAY_SIZE*MAX_ARRAY_SIZE];
    int numTrans = 0;
    std::memset(phases_initial, 0, sizeof(double)*MAX_ARRAY_SIZE*MAX_ARRAY_SIZE);

    // Variables for storing specified points
    double X[N_POINT_SETUPS][MAX_POINTS] = { {0.0}, {-0.015, 0.015}, {0.0, -0.015, 0.015}, {-0.015, 0.015, -0.015, 0.015}};
    double Y[N_POINT_SETUPS][MAX_POINTS] = { {0.0}, {0.0, 0.0}, {0.01, -0.005, 0.005}, {-0.015, -0.015, 0.015, 0.015}};
    double Z[N_POINT_SETUPS][MAX_POINTS] = {{0.065}, {0.065, 0.065}, {0.065, 0.065, 0.065}, {0.065, 0.065, 0.065, 0.065}};
    double P_req[MAX_POINTS] = {0, 0, 0, 0};
    double amplitudes[N_AMPLITUDES] = {1000.0, 1250.0, 1500.0, 1750.0, 2000.0, 2250.0, 2500.0};
    int numPoints[N_POINT_SETUPS] = {1, 2, 3, 4};
    double P_solution[MAX_POINTS];

    // Variables for storing results -- indexing: [array][points][amplitudes]
    double LM_pressure_elapsed[N_ARRAY_SETUPS][N_POINT_SETUPS][N_AMPLITUDES], LM_pressure_error[N_ARRAY_SETUPS][N_POINT_SETUPS][N_AMPLITUDES];
    double LM_pressure_gradient_elapsed[N_ARRAY_SETUPS][N_POINT_SETUPS][N_AMPLITUDES], LM_pressure_gradient_error[N_ARRAY_SETUPS][N_POINT_SETUPS][N_AMPLITUDES];
    double BFGS_elapsed[N_ARRAY_SETUPS][N_POINT_SETUPS][N_AMPLITUDES], BFGS_error[N_ARRAY_SETUPS][N_POINT_SETUPS][N_AMPLITUDES];

    // Output files
    char filename[STRING_BUFFER];
    char row_names[N_POINT_SETUPS][STRING_BUFFER];
    for (int i = 0; i < N_POINT_SETUPS; i++) {
        sprintf(row_names[i], "%d points", numPoints[i]);
    }

    char col_names[N_AMPLITUDES][STRING_BUFFER];
    for (int i = 0; i < N_AMPLITUDES; i++) {
        sprintf(col_names[i], "%.0f Pa", amplitudes[i]);
    }


    // Run benchmark
    generate_lookup();
    for (int i = 0; i < N_ARRAY_SETUPS; i++) {
        int array_size = i + MIN_ARRAY_SIZE;
        numTrans = array_size * array_size;
        generate_array(array_size, tx, ty);
        cout << "Running benchmark for array " << array_size << "x" << array_size << endl;
        for (int j = 0; j < N_POINT_SETUPS; j++) {
            int n = numPoints[j];
            cout << n << " point(s) ";
            double *x = X[j];
            double *y = Y[j];
            double *z = Z[j];            
            for (int k = 0; k < N_AMPLITUDES; k++) {
                fill_vector(P_req, numPoints[j], amplitudes[k]);

                auto tic = chrono::system_clock::now();
                for (int l = 0; l < N_REPEATS; l++) {
                    LM_solve_pressure(x, &n, y, &n, z, &n, P_req, &n, tx, &numTrans, ty, &numTrans, phases, &numTrans);
                }
                auto toc = chrono::system_clock::now();
                chrono::duration<double> elapsed = toc-tic;
                LM_pressure_elapsed[i][j][k] = elapsed.count() / N_REPEATS * 1000.0;
                absolute_pressure_wrapper(x, &n, y, &n, z, &n, tx, &numTrans, ty, &numTrans, phases, &numTrans, P_solution, &n);
                LM_pressure_error[i][j][k] = squared_error(P_req, P_solution, n);

                tic = chrono::system_clock::now();
                for (int l = 0; l < N_REPEATS; l++) {
                    LM_solve_pressure_gradient(x, &n, y, &n, z, &n, P_req, &n, tx, &numTrans, ty, &numTrans, phases, &numTrans);
                }
                toc = chrono::system_clock::now();
                elapsed = toc-tic;
                LM_pressure_gradient_elapsed[i][j][k] = elapsed.count() / N_REPEATS * 1000.0;
                absolute_pressure_wrapper(x, &n, y, &n, z, &n, tx, &numTrans, ty, &numTrans, phases, &numTrans, P_solution, &n);
                LM_pressure_gradient_error[i][j][k] = squared_error(P_req, P_solution, n);

                tic = chrono::system_clock::now();
                for (int l = 0; l < N_REPEATS; l++) {
                    wrapperCustomArray(x, y, z, P_req, n, phases_initial, phases, tx, ty, numTrans);
                }
                toc = chrono::system_clock::now();
                elapsed = toc-tic;
                BFGS_elapsed[i][j][k] = elapsed.count() / N_REPEATS * 1000.0;
                absolute_pressure_wrapper(x, &n, y, &n, z, &n, tx, &numTrans, ty, &numTrans, phases, &numTrans, P_solution, &n);
                BFGS_error[i][j][k] = squared_error(P_req, P_solution, n);
                cout << "#";
            }
            cout << endl;
        }
        cout << endl << "=================" << endl;

        sprintf(filename, "LM_pressure_elapsed_%dx%d.csv", array_size, array_size);
        save_to_csv(filename, LM_pressure_elapsed[i], row_names, col_names);
        sprintf(filename, "LM_pressure_error_%dx%d.csv", array_size, array_size);
        save_to_csv(filename, LM_pressure_error[i], row_names, col_names);

        sprintf(filename, "LM_pressure_gradient_elapsed_%dx%d.csv", array_size, array_size);
        save_to_csv(filename, LM_pressure_gradient_elapsed[i], row_names, col_names);
        sprintf(filename, "LM_pressure_gradient_error_%dx%d.csv", array_size, array_size);
        save_to_csv(filename, LM_pressure_gradient_error[i], row_names, col_names);

        sprintf(filename, "BFGS_elapsed_%dx%d.csv", array_size, array_size);
        save_to_csv(filename, BFGS_elapsed[i], row_names, col_names);
        sprintf(filename, "BFGS_error_%dx%d.csv", array_size, array_size);
        save_to_csv(filename, BFGS_error[i], row_names, col_names);
    }
    
    lookup_terminate();
    return 0;
}