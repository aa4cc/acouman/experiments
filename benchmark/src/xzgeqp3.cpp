//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  xzgeqp3.cpp
//
//  Code generation for function 'xzgeqp3'
//


// Include files
#include "xzgeqp3.h"
#include "LM_solve_gradient.h"
#include "rt_nonfinite.h"
#include "xnrm2.h"
#include <cmath>
#include <cstring>

// Function Declarations
static double rt_hypotd_snf(double u0, double u1);

// Function Definitions
static double rt_hypotd_snf(double u0, double u1)
{
  double y;
  double a;
  a = std::abs(u0);
  y = std::abs(u1);
  if (a < y) {
    a /= y;
    y *= std::sqrt(a * a + 1.0);
  } else if (a > y) {
    y /= a;
    y = a * std::sqrt(y * y + 1.0);
  } else {
    if (!rtIsNaN(y)) {
      y = a * 1.4142135623730951;
    }
  }

  return y;
}

void qrf(double A_data[], const int A_size[2], int m, int n, int nfxd, double
         tau_data[])
{
  int lda;
  int mmip1;
  double work_data[20];
  double atmp;
  double c;
  int jA;
  double beta1;
  int knt;
  lda = A_size[0];
  mmip1 = A_size[1];
  if (0 <= mmip1 - 1) {
    std::memset(&work_data[0], 0, mmip1 * sizeof(double));
  }

  for (int i = 0; i < nfxd; i++) {
    int ii;
    int ix;
    int ix0;
    int lastc;
    int b_i;
    ii = i * lda + i;
    ix = m - i;
    mmip1 = ix - 1;
    if (i + 1 < m) {
      atmp = A_data[ii];
      ix0 = ii + 2;
      tau_data[i] = 0.0;
      if (ix > 0) {
        c = xnrm2(mmip1, A_data, ii + 2);
        if (c != 0.0) {
          beta1 = rt_hypotd_snf(A_data[ii], c);
          if (A_data[ii] >= 0.0) {
            beta1 = -beta1;
          }

          if (std::abs(beta1) < 1.0020841800044864E-292) {
            knt = -1;
            b_i = ii + ix;
            do {
              knt++;
              for (lastc = ix0; lastc <= b_i; lastc++) {
                A_data[lastc - 1] *= 9.9792015476736E+291;
              }

              beta1 *= 9.9792015476736E+291;
              atmp *= 9.9792015476736E+291;
            } while (!(std::abs(beta1) >= 1.0020841800044864E-292));

            beta1 = rt_hypotd_snf(atmp, xnrm2(mmip1, A_data, ii + 2));
            if (atmp >= 0.0) {
              beta1 = -beta1;
            }

            tau_data[i] = (beta1 - atmp) / beta1;
            c = 1.0 / (atmp - beta1);
            for (lastc = ix0; lastc <= b_i; lastc++) {
              A_data[lastc - 1] *= c;
            }

            for (lastc = 0; lastc <= knt; lastc++) {
              beta1 *= 1.0020841800044864E-292;
            }

            atmp = beta1;
          } else {
            tau_data[i] = (beta1 - A_data[ii]) / beta1;
            c = 1.0 / (A_data[ii] - beta1);
            b_i = ii + ix;
            for (lastc = ix0; lastc <= b_i; lastc++) {
              A_data[lastc - 1] *= c;
            }

            atmp = beta1;
          }
        }
      }

      A_data[ii] = atmp;
    } else {
      tau_data[i] = 0.0;
    }

    if (i + 1 < n) {
      int lastv;
      int ia;
      atmp = A_data[ii];
      A_data[ii] = 1.0;
      jA = (ii + lda) + 1;
      if (tau_data[i] != 0.0) {
        bool exitg2;
        lastv = ix - 1;
        mmip1 = (ii + ix) - 1;
        while ((lastv + 1 > 0) && (A_data[mmip1] == 0.0)) {
          lastv--;
          mmip1--;
        }

        lastc = (n - i) - 2;
        exitg2 = false;
        while ((!exitg2) && (lastc + 1 > 0)) {
          int exitg1;
          mmip1 = jA + lastc * lda;
          ia = mmip1;
          do {
            exitg1 = 0;
            if (ia <= mmip1 + lastv) {
              if (A_data[ia - 1] != 0.0) {
                exitg1 = 1;
              } else {
                ia++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        lastv = -1;
        lastc = -1;
      }

      if (lastv + 1 > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work_data[0], 0, (lastc + 1) * sizeof(double));
          }

          mmip1 = 0;
          b_i = jA + lda * lastc;
          for (knt = jA; lda < 0 ? knt >= b_i : knt <= b_i; knt += lda) {
            ix = ii;
            c = 0.0;
            ix0 = knt + lastv;
            for (ia = knt; ia <= ix0; ia++) {
              c += A_data[ia - 1] * A_data[ix];
              ix++;
            }

            work_data[mmip1] += c;
            mmip1++;
          }
        }

        if (!(-tau_data[i] == 0.0)) {
          mmip1 = 0;
          for (ix0 = 0; ix0 <= lastc; ix0++) {
            if (work_data[mmip1] != 0.0) {
              c = work_data[mmip1] * -tau_data[i];
              ix = ii;
              b_i = lastv + jA;
              for (knt = jA; knt <= b_i; knt++) {
                A_data[knt - 1] += A_data[ix] * c;
                ix++;
              }
            }

            mmip1++;
            jA += lda;
          }
        }
      }

      A_data[ii] = atmp;
    }
  }
}

void qrf(coder::array<double, 2U> &A, int m, int n, int nfxd, double tau_data[])
{
  int lda;
  int mmip1;
  double work_data[30];
  double atmp;
  double c;
  int jA;
  double beta1;
  int knt;
  lda = A.size(0);
  mmip1 = A.size(1);
  if (0 <= mmip1 - 1) {
    std::memset(&work_data[0], 0, mmip1 * sizeof(double));
  }

  for (int i = 0; i < nfxd; i++) {
    int ii;
    int mmi_tmp;
    int ix0;
    int b_i;
    int ix;
    ii = i * lda + i;
    mmi_tmp = m - i;
    mmip1 = mmi_tmp - 1;
    if (i + 1 < m) {
      atmp = A[ii];
      ix0 = ii + 2;
      tau_data[i] = 0.0;
      if (mmi_tmp > 0) {
        c = xnrm2(mmip1, A, ii + 2);
        if (c != 0.0) {
          beta1 = rt_hypotd_snf(A[ii], c);
          if (A[ii] >= 0.0) {
            beta1 = -beta1;
          }

          if (std::abs(beta1) < 1.0020841800044864E-292) {
            knt = -1;
            b_i = ii + mmi_tmp;
            do {
              knt++;
              for (ix = ix0; ix <= b_i; ix++) {
                A[ix - 1] = 9.9792015476736E+291 * A[ix - 1];
              }

              beta1 *= 9.9792015476736E+291;
              atmp *= 9.9792015476736E+291;
            } while (!(std::abs(beta1) >= 1.0020841800044864E-292));

            beta1 = rt_hypotd_snf(atmp, xnrm2(mmip1, A, ii + 2));
            if (atmp >= 0.0) {
              beta1 = -beta1;
            }

            tau_data[i] = (beta1 - atmp) / beta1;
            c = 1.0 / (atmp - beta1);
            for (ix = ix0; ix <= b_i; ix++) {
              A[ix - 1] = c * A[ix - 1];
            }

            for (ix = 0; ix <= knt; ix++) {
              beta1 *= 1.0020841800044864E-292;
            }

            atmp = beta1;
          } else {
            tau_data[i] = (beta1 - A[ii]) / beta1;
            c = 1.0 / (A[ii] - beta1);
            b_i = ii + mmi_tmp;
            for (ix = ix0; ix <= b_i; ix++) {
              A[ix - 1] = c * A[ix - 1];
            }

            atmp = beta1;
          }
        }
      }

      A[ii] = atmp;
    } else {
      tau_data[i] = 0.0;
    }

    if (i + 1 < n) {
      int lastv;
      int lastc;
      atmp = A[ii];
      A[ii] = 1.0;
      jA = (ii + lda) + 1;
      if (tau_data[i] != 0.0) {
        bool exitg2;
        lastv = mmi_tmp - 1;
        mmip1 = (ii + mmi_tmp) - 1;
        while ((lastv + 1 > 0) && (A[mmip1] == 0.0)) {
          lastv--;
          mmip1--;
        }

        lastc = (n - i) - 2;
        exitg2 = false;
        while ((!exitg2) && (lastc + 1 > 0)) {
          int exitg1;
          mmip1 = jA + lastc * lda;
          mmi_tmp = mmip1;
          do {
            exitg1 = 0;
            if (mmi_tmp <= mmip1 + lastv) {
              if (A[mmi_tmp - 1] != 0.0) {
                exitg1 = 1;
              } else {
                mmi_tmp++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        lastv = -1;
        lastc = -1;
      }

      if (lastv + 1 > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work_data[0], 0, (lastc + 1) * sizeof(double));
          }

          mmip1 = 0;
          b_i = jA + lda * lastc;
          for (knt = jA; lda < 0 ? knt >= b_i : knt <= b_i; knt += lda) {
            ix = ii;
            c = 0.0;
            ix0 = knt + lastv;
            for (mmi_tmp = knt; mmi_tmp <= ix0; mmi_tmp++) {
              c += A[mmi_tmp - 1] * A[ix];
              ix++;
            }

            work_data[mmip1] += c;
            mmip1++;
          }
        }

        if (!(-tau_data[i] == 0.0)) {
          mmip1 = 0;
          for (ix0 = 0; ix0 <= lastc; ix0++) {
            if (work_data[mmip1] != 0.0) {
              c = work_data[mmip1] * -tau_data[i];
              ix = ii;
              b_i = lastv + jA;
              for (knt = jA; knt <= b_i; knt++) {
                A[knt - 1] = A[knt - 1] + A[ix] * c;
                ix++;
              }
            }

            mmip1++;
            jA += lda;
          }
        }
      }

      A[ii] = atmp;
    }
  }
}

// End of code generation (xzgeqp3.cpp)
