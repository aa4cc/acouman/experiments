//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  pressure_derivative_jacobian.cpp
//
//  Code generation for function 'pressure_derivative_jacobian'
//


// Include files
#include "pressure_derivative_jacobian.h"
#include "LM_solve_gradient.h"
#include "rtwutil.h"
#include "rt_nonfinite.h"
#include <cstring>

// Function Definitions
void pressure_derivative_jacobian(const double M_data[], const int M_size[2],
  const double Mx_data[], const int Mx_size[2], const double My_data[], const
  int My_size[2], const double c_data[], const double s_data[], double Jx_data[],
  int Jx_size[2], double Jy_data[], int Jy_size[2])
{
  int num_pp;
  int loop_ub_tmp;
  int varargin_1_size_idx_0;
  int b_varargin_1_size_idx_0;
  int c_varargin_1_size_idx_0;
  int loop_ub;
  int b_loop_ub;
  double p_data[512];
  double px_data[512];
  double pc_idx_0;
  double py_data[512];
  double pc_idx_1;
  double ps_idx_0;
  double ps_idx_1;
  double pxc_idx_0;
  double pxc_idx_1;
  double pxs_idx_0;
  double pxs_idx_1;
  double pyc_idx_0;
  double pyc_idx_1;
  double pys_idx_0;
  double pys_idx_1;
  double varargin_1_data[256];
  double C_data[256];
  double b_C_data[256];
  double c_C_data[256];
  num_pp = static_cast<int>(static_cast<unsigned int>(rt_roundd_snf(static_cast<
    double>(M_size[1]) / 2.0)));
  Jx_size[0] = num_pp;
  Jx_size[1] = M_size[0];
  loop_ub_tmp = num_pp * M_size[0];
  if (0 <= loop_ub_tmp - 1) {
    std::memset(&Jx_data[0], 0, loop_ub_tmp * sizeof(double));
  }

  Jy_size[0] = num_pp;
  Jy_size[1] = M_size[0];
  if (0 <= loop_ub_tmp - 1) {
    std::memset(&Jy_data[0], 0, loop_ub_tmp * sizeof(double));
  }

  if (0 <= num_pp - 1) {
    varargin_1_size_idx_0 = M_size[0];
    b_varargin_1_size_idx_0 = Mx_size[0];
    c_varargin_1_size_idx_0 = My_size[0];
    loop_ub = Jx_size[1];
    b_loop_ub = Jy_size[1];
  }

  for (int i = 0; i < num_pp; i++) {
    int b_i;
    double unnamed_idx_1;
    double unnamed_idx_0;
    double b_unnamed_idx_1;
    loop_ub_tmp = static_cast<int>(((i + 1U) << 1));
    for (b_i = 0; b_i < varargin_1_size_idx_0; b_i++) {
      p_data[b_i] = M_data[b_i + M_size[0] * (loop_ub_tmp - 2)];
      p_data[b_i + varargin_1_size_idx_0] = M_data[b_i + M_size[0] *
        (loop_ub_tmp - 1)];
    }

    for (b_i = 0; b_i < b_varargin_1_size_idx_0; b_i++) {
      px_data[b_i] = Mx_data[b_i + Mx_size[0] * (loop_ub_tmp - 2)];
      px_data[b_i + b_varargin_1_size_idx_0] = Mx_data[b_i + Mx_size[0] *
        (loop_ub_tmp - 1)];
    }

    for (b_i = 0; b_i < c_varargin_1_size_idx_0; b_i++) {
      py_data[b_i] = My_data[b_i + My_size[0] * (loop_ub_tmp - 2)];
      py_data[b_i + c_varargin_1_size_idx_0] = My_data[b_i + My_size[0] *
        (loop_ub_tmp - 1)];
    }

    pc_idx_0 = 0.0;
    pc_idx_1 = 0.0;
    ps_idx_0 = 0.0;
    ps_idx_1 = 0.0;
    for (loop_ub_tmp = 0; loop_ub_tmp < varargin_1_size_idx_0; loop_ub_tmp++) {
      pc_idx_0 += p_data[loop_ub_tmp] * c_data[loop_ub_tmp];
      unnamed_idx_1 = p_data[varargin_1_size_idx_0 + loop_ub_tmp];
      pc_idx_1 += unnamed_idx_1 * c_data[loop_ub_tmp];
      ps_idx_0 += p_data[loop_ub_tmp] * s_data[loop_ub_tmp];
      ps_idx_1 += unnamed_idx_1 * s_data[loop_ub_tmp];
    }

    pxc_idx_0 = 0.0;
    pxc_idx_1 = 0.0;
    pxs_idx_0 = 0.0;
    pxs_idx_1 = 0.0;
    for (loop_ub_tmp = 0; loop_ub_tmp < b_varargin_1_size_idx_0; loop_ub_tmp++)
    {
      pxc_idx_0 += px_data[loop_ub_tmp] * c_data[loop_ub_tmp];
      unnamed_idx_1 = px_data[b_varargin_1_size_idx_0 + loop_ub_tmp];
      pxc_idx_1 += unnamed_idx_1 * c_data[loop_ub_tmp];
      pxs_idx_0 += px_data[loop_ub_tmp] * s_data[loop_ub_tmp];
      pxs_idx_1 += unnamed_idx_1 * s_data[loop_ub_tmp];
    }

    pyc_idx_0 = 0.0;
    pyc_idx_1 = 0.0;
    pys_idx_0 = 0.0;
    pys_idx_1 = 0.0;
    for (loop_ub_tmp = 0; loop_ub_tmp < c_varargin_1_size_idx_0; loop_ub_tmp++)
    {
      pyc_idx_0 += py_data[loop_ub_tmp] * c_data[loop_ub_tmp];
      unnamed_idx_1 = py_data[c_varargin_1_size_idx_0 + loop_ub_tmp];
      pyc_idx_1 += unnamed_idx_1 * c_data[loop_ub_tmp];
      pys_idx_0 += py_data[loop_ub_tmp] * s_data[loop_ub_tmp];
      pys_idx_1 += unnamed_idx_1 * s_data[loop_ub_tmp];
    }

    unnamed_idx_0 = pc_idx_0 + -ps_idx_1;
    b_unnamed_idx_1 = pc_idx_1 + ps_idx_0;
    ps_idx_0 -= -pc_idx_1;
    unnamed_idx_1 = ps_idx_1 - pc_idx_0;
    for (loop_ub_tmp = 0; loop_ub_tmp < b_varargin_1_size_idx_0; loop_ub_tmp++)
    {
      varargin_1_data[loop_ub_tmp] = px_data[loop_ub_tmp] * unnamed_idx_0 +
        px_data[b_varargin_1_size_idx_0 + loop_ub_tmp] * b_unnamed_idx_1;
    }

    pc_idx_0 = pxc_idx_0 + -pxs_idx_1;
    pc_idx_1 = pxc_idx_1 + pxs_idx_0;
    for (loop_ub_tmp = 0; loop_ub_tmp < varargin_1_size_idx_0; loop_ub_tmp++) {
      C_data[loop_ub_tmp] = p_data[loop_ub_tmp] * pc_idx_0 +
        p_data[varargin_1_size_idx_0 + loop_ub_tmp] * pc_idx_1;
    }

    for (loop_ub_tmp = 0; loop_ub_tmp < b_varargin_1_size_idx_0; loop_ub_tmp++)
    {
      b_C_data[loop_ub_tmp] = px_data[loop_ub_tmp] * ps_idx_0 +
        px_data[b_varargin_1_size_idx_0 + loop_ub_tmp] * unnamed_idx_1;
    }

    pc_idx_0 = pxs_idx_0 - (-pxc_idx_1);
    pc_idx_1 = pxs_idx_1 - pxc_idx_0;
    for (loop_ub_tmp = 0; loop_ub_tmp < varargin_1_size_idx_0; loop_ub_tmp++) {
      c_C_data[loop_ub_tmp] = p_data[loop_ub_tmp] * pc_idx_0 +
        p_data[varargin_1_size_idx_0 + loop_ub_tmp] * pc_idx_1;
    }

    for (loop_ub_tmp = 0; loop_ub_tmp < loop_ub; loop_ub_tmp++) {
      Jx_data[i + num_pp * loop_ub_tmp] = 2.0 * (-s_data[loop_ub_tmp] *
        (varargin_1_data[loop_ub_tmp] + C_data[loop_ub_tmp]) +
        c_data[loop_ub_tmp] * (b_C_data[loop_ub_tmp] + c_C_data[loop_ub_tmp]));
    }

    for (loop_ub_tmp = 0; loop_ub_tmp < c_varargin_1_size_idx_0; loop_ub_tmp++)
    {
      varargin_1_data[loop_ub_tmp] = py_data[loop_ub_tmp] * unnamed_idx_0 +
        py_data[c_varargin_1_size_idx_0 + loop_ub_tmp] * b_unnamed_idx_1;
    }

    pc_idx_0 = pyc_idx_0 + -pys_idx_1;
    pc_idx_1 = pyc_idx_1 + pys_idx_0;
    for (loop_ub_tmp = 0; loop_ub_tmp < varargin_1_size_idx_0; loop_ub_tmp++) {
      C_data[loop_ub_tmp] = p_data[loop_ub_tmp] * pc_idx_0 +
        p_data[varargin_1_size_idx_0 + loop_ub_tmp] * pc_idx_1;
    }

    for (loop_ub_tmp = 0; loop_ub_tmp < c_varargin_1_size_idx_0; loop_ub_tmp++)
    {
      b_C_data[loop_ub_tmp] = py_data[loop_ub_tmp] * ps_idx_0 +
        py_data[c_varargin_1_size_idx_0 + loop_ub_tmp] * unnamed_idx_1;
    }

    pc_idx_0 = pys_idx_0 - (-pyc_idx_1);
    pc_idx_1 = pys_idx_1 - pyc_idx_0;
    for (loop_ub_tmp = 0; loop_ub_tmp < varargin_1_size_idx_0; loop_ub_tmp++) {
      c_C_data[loop_ub_tmp] = p_data[loop_ub_tmp] * pc_idx_0 +
        p_data[varargin_1_size_idx_0 + loop_ub_tmp] * pc_idx_1;
    }

    for (loop_ub_tmp = 0; loop_ub_tmp < b_loop_ub; loop_ub_tmp++) {
      Jy_data[i + num_pp * loop_ub_tmp] = 2.0 * (-s_data[loop_ub_tmp] *
        (varargin_1_data[loop_ub_tmp] + C_data[loop_ub_tmp]) +
        c_data[loop_ub_tmp] * (b_C_data[loop_ub_tmp] + c_C_data[loop_ub_tmp]));
    }
  }
}

// End of code generation (pressure_derivative_jacobian.cpp)
