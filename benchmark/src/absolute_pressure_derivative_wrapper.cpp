//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_derivative_wrapper.cpp
//
//  Code generation for function 'absolute_pressure_derivative_wrapper'
//


// Include files
#include "absolute_pressure_derivative_wrapper.h"
#include "absolute_pressure_derivative.h"
#include "generate_lookup_data.h"
#include "generate_lookup.h"
#include "interp1.h"
#include "rt_nonfinite.h"
#include <cmath>
#include <cstring>
#include <math.h>

// Function Declarations
static double rt_powd_snf(double u0, double u1);

// Function Definitions
static double rt_powd_snf(double u0, double u1)
{
  double y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    double d;
    double d1;
    d = std::abs(u0);
    d1 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (d == 1.0) {
        y = 1.0;
      } else if (d > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void absolute_pressure_derivative_wrapper(const double x_data[], const int
  x_size[1], const double y_data[], const int [1], const double z_data[], const
  int [1], const double tx_data[], const int tx_size[1], const double ty_data[],
  const int [1], const double phases_data[], const int phases_size[1], double
  Px_data[], int Px_size[1], double Py_data[], int Py_size[1])
{
  int M_size[2];
  int nx;
  int loop_ub_tmp;
  double M_data[5120];
  int i;
  int b_i;
  int i1;
  int j;
  int Mx_size[2];
  double xr;
  double Mx_data[5120];
  double yr;
  int My_size[2];
  double p;
  double d;
  double My_data[5120];
  double phase_shift;
  int c_size[1];
  double c_data[256];
  int s_size[1];
  double s_data[256];
  if (!isInitialized_lookup) {
    generate_lookup();
  }

  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  M_size[0] = tx_size[0];
  nx = x_size[0] << 1;
  M_size[1] = nx;
  loop_ub_tmp = tx_size[0] * nx;
  if (0 <= loop_ub_tmp - 1) {
    std::memset(&M_data[0], 0, loop_ub_tmp * sizeof(double));
  }

  i = x_size[0];
  for (b_i = 0; b_i < i; b_i++) {
    //  iterate over points
    i1 = tx_size[0];
    for (j = 0; j < i1; j++) {
      int M_tmp;

      //  iterate over transducers
      xr = x_data[b_i] - tx_data[j];
      yr = y_data[b_i] - ty_data[j];
      p = xr * xr + yr * yr;
      d = std::sqrt(p + z_data[b_i] * z_data[b_i]);

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      p = 5.0 * interp1(sinAngle, directivity, std::sqrt(p) / d) / d;
      phase_shift = 732.733 * d;
      M_tmp = (b_i + 1) << 1;
      M_data[j + M_size[0] * (M_tmp - 2)] = p * std::cos(phase_shift);
      M_data[j + M_size[0] * (M_tmp - 1)] = p * std::sin(phase_shift);
    }
  }

  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  Mx_size[0] = tx_size[0];
  Mx_size[1] = nx;
  if (0 <= loop_ub_tmp - 1) {
    std::memset(&Mx_data[0], 0, loop_ub_tmp * sizeof(double));
  }

  My_size[0] = tx_size[0];
  My_size[1] = nx;
  if (0 <= loop_ub_tmp - 1) {
    std::memset(&My_data[0], 0, loop_ub_tmp * sizeof(double));
  }

  i = x_size[0];
  for (b_i = 0; b_i < i; b_i++) {
    //  iterate over points
    i1 = tx_size[0];
    for (j = 0; j < i1; j++) {
      double d_tmp;
      double f_dir;
      double pxr_tmp;
      double pxr;
      double pxi_tmp;
      double b_pxi_tmp;
      double pxi;

      //  iterate over transducers
      xr = x_data[b_i] - tx_data[j];
      yr = y_data[b_i] - ty_data[j];
      p = xr * xr + yr * yr;
      d_tmp = z_data[b_i] * z_data[b_i];
      d = std::sqrt(p + d_tmp);
      p = std::sqrt(p) / d;

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      f_dir = interp1(sinAngle, directivity, p);
      p = interp1(sinAngle, directivity_derivative, p);
      phase_shift = rt_powd_snf(d, 5.0);
      pxr_tmp = rt_powd_snf(d, 3.0);
      pxr = 5.0 * (p * xr * d_tmp / phase_shift - f_dir * xr / pxr_tmp);
      pxi_tmp = 5.0 * f_dir * 732.733;
      b_pxi_tmp = d * d;
      pxi = pxi_tmp * xr / b_pxi_tmp;
      f_dir = 5.0 * (p * yr * d_tmp / phase_shift - f_dir * yr / pxr_tmp);
      xr = pxi_tmp * yr / b_pxi_tmp;
      phase_shift = 732.733 * d;
      pxr_tmp = std::sin(phase_shift);
      p = std::cos(phase_shift);
      nx = (b_i + 1) << 1;
      loop_ub_tmp = nx - 2;
      Mx_data[j + Mx_size[0] * loop_ub_tmp] = pxr * p - pxi * pxr_tmp;
      nx--;
      Mx_data[j + Mx_size[0] * nx] = pxr * pxr_tmp + pxi * p;
      My_data[j + My_size[0] * loop_ub_tmp] = f_dir * p - xr * pxr_tmp;
      My_data[j + My_size[0] * nx] = f_dir * pxr_tmp + xr * p;
    }
  }

  c_size[0] = phases_size[0];
  nx = phases_size[0];
  if (0 <= nx - 1) {
    std::memcpy(&c_data[0], &phases_data[0], nx * sizeof(double));
  }

  nx = phases_size[0];
  for (loop_ub_tmp = 0; loop_ub_tmp < nx; loop_ub_tmp++) {
    c_data[loop_ub_tmp] = std::cos(c_data[loop_ub_tmp]);
  }

  s_size[0] = phases_size[0];
  nx = phases_size[0];
  if (0 <= nx - 1) {
    std::memcpy(&s_data[0], &phases_data[0], nx * sizeof(double));
  }

  nx = phases_size[0];
  for (loop_ub_tmp = 0; loop_ub_tmp < nx; loop_ub_tmp++) {
    s_data[loop_ub_tmp] = std::sin(s_data[loop_ub_tmp]);
  }

  absolute_pressure_derivative(M_data, M_size, Mx_data, Mx_size, My_data,
    My_size, c_data, c_size, s_data, s_size, Px_data, Px_size, Py_data, Py_size);
}

// End of code generation (absolute_pressure_derivative_wrapper.cpp)
