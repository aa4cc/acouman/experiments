//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  LM_solve_pressure.cpp
//
//  Code generation for function 'LM_solve_pressure'
//


// Include files
#include "LM_solve_pressure.h"
#include "generate_lookup_data.h"
#include "generate_lookup.h"
#include "absolute_pressure.h"
#include "p.h"
#include "pressure_jacobian.h"
#include "qr.h"
#include "rt_nonfinite.h"
#include "trisolve.h"
#include <cmath>
#include <cstring>

// Function Definitions
void LM_solve_pressure(const double x_data[], const int x_size[1], const double
  y_data[], const int [1], const double z_data[], const int [1], const double
  reqP_data[], const int reqP_size[1], const double tx_data[], const int
  tx_size[1], const double ty_data[], const int [1], double phases_data[], int
  phases_size[1])
{
  int aoffset;
  double M_data[5120];
  int M_size[2];
  int c_size[1];
  double c_data[256];
  int nx;
  int nA;
  int s_size[1];
  double s_data[256];
  double o_data[10];
  int o_size[1];
  int obj_fit_size_idx_0;
  double penalty;
  double obj_fit_data[10];
  int i;
  double J_data[2560];
  int J_size[2];
  double obj_fit_tmp;
  int nump_pp;
  double mu;
  int b_i;
  bool exitg1;
  double rhs_data[10];
  double b_data[100];
  double varargin_1_tmp_data[2560];
  int R_size[2];
  double R_data[2660];
  double b_R_data[2660];
  int b_size[2];
  int new_o_size[1];
  double new_o_data[10];
  int tmp_size[1];
  double tmp_data[10];
  int new_s_size[1];
  double new_s_data[256];
  double obj_fit;
  if (!isInitialized_lookup) {
    generate_lookup();
  }

  //     %% Preparation
  //      phases = pi*randn(array_size^2,1); % initial guess
  phases_size[0] = tx_size[0];
  aoffset = tx_size[0];
  if (0 <= aoffset - 1) {
    std::memset(&phases_data[0], 0, aoffset * sizeof(double));
  }

  //  initial guess
  //  calculate acoustic pressure
  p(x_data, x_size, y_data, z_data, tx_data, tx_size, ty_data, M_data, M_size);
  c_size[0] = tx_size[0];
  aoffset = tx_size[0];
  if (0 <= aoffset - 1) {
    std::memset(&c_data[0], 0, aoffset * sizeof(double));
  }

  nx = tx_size[0];
  for (nA = 0; nA < nx; nA++) {
    c_data[nA] = std::cos(c_data[nA]);
  }

  s_size[0] = tx_size[0];
  aoffset = tx_size[0];
  if (0 <= aoffset - 1) {
    std::memset(&s_data[0], 0, aoffset * sizeof(double));
  }

  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&s_data[0], 0, nx * sizeof(double));
  }

  absolute_pressure(M_data, M_size, c_data, c_size, s_data, s_size, o_data,
                    o_size);
  obj_fit_size_idx_0 = reqP_size[0];
  nx = reqP_size[0];
  for (nA = 0; nA < nx; nA++) {
    obj_fit_data[nA] = reqP_data[nA] * reqP_data[nA];
  }

  penalty = 0.0;
  for (i = 0; i < obj_fit_size_idx_0; i++) {
    obj_fit_tmp = obj_fit_data[i] - o_data[i];
    penalty += obj_fit_tmp * obj_fit_tmp;
  }

  pressure_jacobian(M_data, M_size, c_data, c_size, s_data, s_size, J_data,
                    J_size);
  nump_pp = x_size[0];

  //     %% Levenberg-Marquardt solver
  mu = 1.0;

  //  initial damping
  //      fprintf('|Iteration|Penalty |Damping |LSCount|\n');
  //      fprintf('|---------|--------|--------|-------|\n');
  b_i = 0;
  exitg1 = false;
  while ((!exitg1) && (b_i < 7)) {
    int num_search;

    //  number of iterations
    aoffset = o_size[0];
    for (i = 0; i < aoffset; i++) {
      rhs_data[i] = o_data[i] - obj_fit_data[i];
    }

    num_search = 1;
    int exitg2;
    do {
      int varargin_1_tmp_size_idx_0;
      int varargin_1_tmp_size_idx_1;
      int i1;
      bool empty_non_axis_sizes;
      short input_sizes_idx_0;
      signed char b_input_sizes_idx_0;
      int b_nA;
      exitg2 = 0;
      obj_fit_tmp = std::sqrt(mu);
      aoffset = nump_pp * nump_pp;
      if (0 <= aoffset - 1) {
        std::memset(&b_data[0], 0, aoffset * sizeof(double));
      }

      if (nump_pp > 0) {
        for (nA = 0; nA < nump_pp; nA++) {
          b_data[nA + nump_pp * nA] = 1.0;
        }
      }

      aoffset = nump_pp * nump_pp;
      for (i = 0; i < aoffset; i++) {
        b_data[i] *= obj_fit_tmp;
      }

      varargin_1_tmp_size_idx_0 = J_size[1];
      varargin_1_tmp_size_idx_1 = J_size[0];
      aoffset = J_size[0];
      for (i = 0; i < aoffset; i++) {
        nx = J_size[1];
        for (i1 = 0; i1 < nx; i1++) {
          varargin_1_tmp_data[i1 + varargin_1_tmp_size_idx_0 * i] = J_data[i +
            J_size[0] * i1];
        }
      }

      if ((J_size[1] != 0) && (J_size[0] != 0)) {
        nx = J_size[0];
      } else if (nump_pp != 0) {
        nx = nump_pp;
      } else {
        nx = J_size[0];
        if (nx <= 0) {
          nx = 0;
        }
      }

      empty_non_axis_sizes = (nx == 0);
      if (empty_non_axis_sizes || ((J_size[1] != 0) && (J_size[0] != 0))) {
        input_sizes_idx_0 = static_cast<short>(J_size[1]);
      } else {
        input_sizes_idx_0 = 0;
      }

      if (empty_non_axis_sizes || (nump_pp != 0)) {
        b_input_sizes_idx_0 = static_cast<signed char>(nump_pp);
      } else {
        b_input_sizes_idx_0 = 0;
      }

      //              [~, R] = qr(Abar, 0);
      aoffset = input_sizes_idx_0;
      b_nA = b_input_sizes_idx_0;
      R_size[0] = input_sizes_idx_0 + b_input_sizes_idx_0;
      R_size[1] = nx;
      for (i = 0; i < nx; i++) {
        for (i1 = 0; i1 < aoffset; i1++) {
          R_data[i1 + R_size[0] * i] = varargin_1_tmp_data[i1 +
            input_sizes_idx_0 * i];
        }
      }

      for (i = 0; i < nx; i++) {
        for (i1 = 0; i1 < b_nA; i1++) {
          R_data[(i1 + input_sizes_idx_0) + R_size[0] * i] = b_data[i1 +
            b_input_sizes_idx_0 * i];
        }
      }

      qr(R_data, R_size);
      if (1 > nump_pp) {
        nA = 0;
        b_nA = 0;
      } else {
        nA = nump_pp;
        b_nA = nump_pp;
      }

      for (i = 0; i < b_nA; i++) {
        for (i1 = 0; i1 < nA; i1++) {
          b_R_data[i1 + nA * i] = R_data[i1 + R_size[0] * i];
        }
      }

      R_size[0] = nA;
      R_size[1] = b_nA;
      aoffset = nA * b_nA;
      if (0 <= aoffset - 1) {
        std::memcpy(&R_data[0], &b_R_data[0], aoffset * sizeof(double));
      }

      //              step = -J'*((R'*R)\rhs);
      for (i = 0; i < nA; i++) {
        for (i1 = 0; i1 < b_nA; i1++) {
          b_R_data[i1 + b_nA * i] = R_data[i + nA * i1];
        }
      }

      b_size[0] = b_nA;
      b_size[1] = nA;
      aoffset = b_nA * nA;
      if (0 <= aoffset - 1) {
        std::memcpy(&b_data[0], &b_R_data[0], aoffset * sizeof(double));
      }

      if (b_nA < nA) {
        nx = b_nA;
      } else {
        nx = nA;
      }

      new_o_size[0] = nA;
      if (0 <= nx - 1) {
        std::memcpy(&new_o_data[0], &rhs_data[0], nx * sizeof(double));
      }

      i = nx + 1;
      if (i <= nA) {
        std::memset(&new_o_data[i + -1], 0, ((nA - i) + 1) * sizeof(double));
      }

      trisolve(b_data, b_size, new_o_data, new_o_size);
      if (nA >= b_nA) {
        nA = b_nA;
      }

      tmp_size[0] = b_nA;
      if (0 <= nA - 1) {
        std::memcpy(&tmp_data[0], &new_o_data[0], nA * sizeof(double));
      }

      i = nA + 1;
      if (i <= b_nA) {
        std::memset(&tmp_data[i + -1], 0, ((b_nA - i) + 1) * sizeof(double));
      }

      b_trisolve(R_data, R_size, tmp_data, tmp_size);
      aoffset = J_size[1] * J_size[0];
      for (i = 0; i < aoffset; i++) {
        varargin_1_tmp_data[i] = -varargin_1_tmp_data[i];
      }

      nx = J_size[1] - 1;
      if (0 <= nx) {
        std::memset(&c_data[0], 0, (nx + 1) * sizeof(double));
      }

      for (nA = 0; nA < varargin_1_tmp_size_idx_1; nA++) {
        aoffset = nA * varargin_1_tmp_size_idx_0;
        for (b_nA = 0; b_nA <= nx; b_nA++) {
          c_data[b_nA] += varargin_1_tmp_data[aoffset + b_nA] * tmp_data[nA];
        }
      }

      c_size[0] = phases_size[0];
      aoffset = phases_size[0];
      for (i = 0; i < aoffset; i++) {
        c_data[i] += phases_data[i];
      }

      s_size[0] = phases_size[0];
      aoffset = phases_size[0];
      if (0 <= aoffset - 1) {
        std::memcpy(&s_data[0], &c_data[0], aoffset * sizeof(double));
      }

      nx = phases_size[0];
      for (nA = 0; nA < nx; nA++) {
        s_data[nA] = std::cos(s_data[nA]);
      }

      new_s_size[0] = phases_size[0];
      aoffset = phases_size[0];
      if (0 <= aoffset - 1) {
        std::memcpy(&new_s_data[0], &c_data[0], aoffset * sizeof(double));
      }

      nx = phases_size[0];
      for (nA = 0; nA < nx; nA++) {
        new_s_data[nA] = std::sin(new_s_data[nA]);
      }

      absolute_pressure(M_data, M_size, s_data, s_size, new_s_data, new_s_size,
                        new_o_data, new_o_size);
      obj_fit = 0.0;
      for (i = 0; i < obj_fit_size_idx_0; i++) {
        obj_fit_tmp = obj_fit_data[i] - new_o_data[i];
        obj_fit += obj_fit_tmp * obj_fit_tmp;
      }

      if (obj_fit < penalty) {
        aoffset = c_size[0];
        if (0 <= aoffset - 1) {
          std::memcpy(&phases_data[0], &c_data[0], aoffset * sizeof(double));
        }

        o_size[0] = new_o_size[0];
        aoffset = new_o_size[0];
        if (0 <= aoffset - 1) {
          std::memcpy(&o_data[0], &new_o_data[0], aoffset * sizeof(double));
        }

        penalty = obj_fit;

        //                  fprintf('|%9d|%8.2e|%8.2e|%7d|\n',i,penalty,mu,num_search); 
        pressure_jacobian(M_data, M_size, s_data, s_size, new_s_data, new_s_size,
                          J_data, J_size);
        mu /= 2.0;
        exitg2 = 1;
      } else {
        mu *= 2.0;
        num_search = static_cast<int>((num_search + 1U));
        if (num_search > 100) {
          //                 fprintf('LMsolve failed to converge on iteration %u\n',i); 
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    if (penalty < 1.0) {
      //  penalty tolerance
      exitg1 = true;
    } else {
      b_i++;
    }
  }
}

// End of code generation (LM_solve_pressure.cpp)
