//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  LM_solve_gradient.cpp
//
//  Code generation for function 'LM_solve_gradient'
//


// Include files
#include "LM_solve_gradient.h"
#include "generate_lookup_data.h"
#include "generate_lookup.h"
#include "absolute_pressure_derivative.h"
#include "interp1.h"
#include "pressure_derivative_jacobian.h"
#include "qr.h"
#include "rt_nonfinite.h"
#include "trisolve.h"
#include <cmath>
#include <cstring>
#include <math.h>

// Function Declarations
static double rt_powd_snf(double u0, double u1);

// Function Definitions
static double rt_powd_snf(double u0, double u1)
{
  double y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    double d;
    double d1;
    d = std::abs(u0);
    d1 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (d == 1.0) {
        y = 1.0;
      } else if (d > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void LM_solve_gradient(const double x_data[], const int x_size[1], const double
  y_data[], const int [1], const double z_data[], const int [1], const double
  reqPx_data[], const int reqPx_size[1], const double reqPy_data[], const int
  reqPy_size[1], const double tx_data[], const int tx_size[1], const double
  ty_data[], const int [1], double phases_data[], int phases_size[1])
{
  int nx;
  int M_size[2];
  int M_size_tmp;
  int minszA;
  static double M_data[5120];
  int i;
  int b_i;
  int i1;
  int nA;
  int Mx_size[2];
  double xr;
  static double Mx_data[5120];
  double yr;
  int My_size[2];
  double p;
  double d;
  static double My_data[5120];
  double phase_shift;
  int c_size[1];
  double c_data[256];
  int b_nA;
  int s_size[1];
  double s_data[256];
  double Px_data[10];
  int Px_size[1];
  double Py_data[10];
  int Py_size[1];
  int o_size_idx_0;
  double penalty;
  double o_data[20];
  int obj_fit_size_idx_0;
  double obj_fit_data[20];
  double Jx_data[2560];
  int Jx_size[2];
  double Jy_data[2560];
  int Jy_size[2];
  int u0;
  bool empty_non_axis_sizes;
  signed char input_sizes_idx_0;
  signed char sizes_idx_0;
  int J_size_idx_0;
  static double J_data[5120];
  bool exitg1;
  double rhs_data[20];
  double I_data[400];
  double varargin_1_tmp_data[5120];
  double varargin_2_data[5120];
  int R_size[2];
  static double R_data[5520];
  static double b_R_data[5520];
  int I_size[2];
  int new_o_size[1];
  double new_o_data[20];
  int C_size[1];
  double C_data[20];
  int new_s_size[1];
  double new_s_data[256];
  if (!isInitialized_lookup) {
    generate_lookup();
  }

  //     %% Preparation
  //      phases = pi*randn(array_size^2,1); % initial guess
  phases_size[0] = tx_size[0];
  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&phases_data[0], 0, nx * sizeof(double));
  }

  //  initial guess
  //  calculate acoustic pressure
  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  M_size[0] = tx_size[0];
  M_size_tmp = x_size[0] << 1;
  M_size[1] = M_size_tmp;
  minszA = tx_size[0] * M_size_tmp;
  if (0 <= minszA - 1) {
    std::memset(&M_data[0], 0, minszA * sizeof(double));
  }

  i = x_size[0];
  for (b_i = 0; b_i < i; b_i++) {
    //  iterate over points
    i1 = tx_size[0];
    for (nA = 0; nA < i1; nA++) {
      //  iterate over transducers
      xr = x_data[b_i] - tx_data[nA];
      yr = y_data[b_i] - ty_data[nA];
      p = xr * xr + yr * yr;
      d = std::sqrt(p + z_data[b_i] * z_data[b_i]);

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      p = 5.0 * interp1(sinAngle, directivity, std::sqrt(p) / d) / d;
      phase_shift = 732.733 * d;
      nx = (b_i + 1) << 1;
      M_data[nA + M_size[0] * (nx - 2)] = p * std::cos(phase_shift);
      M_data[nA + M_size[0] * (nx - 1)] = p * std::sin(phase_shift);
    }
  }

  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  Mx_size[0] = tx_size[0];
  Mx_size[1] = M_size_tmp;
  if (0 <= minszA - 1) {
    std::memset(&Mx_data[0], 0, minszA * sizeof(double));
  }

  My_size[0] = tx_size[0];
  My_size[1] = M_size_tmp;
  if (0 <= minszA - 1) {
    std::memset(&My_data[0], 0, minszA * sizeof(double));
  }

  i = x_size[0];
  for (b_i = 0; b_i < i; b_i++) {
    //  iterate over points
    i1 = tx_size[0];
    for (nA = 0; nA < i1; nA++) {
      double d_tmp;
      double f_dir;
      double pxr;
      double pxi_tmp;
      double b_pxi_tmp;
      double pxi;

      //  iterate over transducers
      xr = x_data[b_i] - tx_data[nA];
      yr = y_data[b_i] - ty_data[nA];
      p = xr * xr + yr * yr;
      d_tmp = z_data[b_i] * z_data[b_i];
      d = std::sqrt(p + d_tmp);
      p = std::sqrt(p) / d;

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      f_dir = interp1(sinAngle, directivity, p);
      p = interp1(sinAngle, directivity_derivative, p);
      penalty = rt_powd_snf(d, 5.0);
      phase_shift = rt_powd_snf(d, 3.0);
      pxr = 5.0 * (p * xr * d_tmp / penalty - f_dir * xr / phase_shift);
      pxi_tmp = 5.0 * f_dir * 732.733;
      b_pxi_tmp = d * d;
      pxi = pxi_tmp * xr / b_pxi_tmp;
      f_dir = 5.0 * (p * yr * d_tmp / penalty - f_dir * yr / phase_shift);
      xr = pxi_tmp * yr / b_pxi_tmp;
      phase_shift = 732.733 * d;
      penalty = std::sin(phase_shift);
      p = std::cos(phase_shift);
      nx = (b_i + 1) << 1;
      minszA = nx - 2;
      Mx_data[nA + Mx_size[0] * minszA] = pxr * p - pxi * penalty;
      nx--;
      Mx_data[nA + Mx_size[0] * nx] = pxr * penalty + pxi * p;
      My_data[nA + My_size[0] * minszA] = f_dir * p - xr * penalty;
      My_data[nA + My_size[0] * nx] = f_dir * penalty + xr * p;
    }
  }

  c_size[0] = tx_size[0];
  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&c_data[0], 0, nx * sizeof(double));
  }

  nx = tx_size[0];
  for (b_nA = 0; b_nA < nx; b_nA++) {
    c_data[b_nA] = std::cos(c_data[b_nA]);
  }

  s_size[0] = tx_size[0];
  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&s_data[0], 0, nx * sizeof(double));
  }

  nx = tx_size[0];
  if (0 <= nx - 1) {
    std::memset(&s_data[0], 0, nx * sizeof(double));
  }

  absolute_pressure_derivative(M_data, M_size, Mx_data, Mx_size, My_data,
    My_size, c_data, c_size, s_data, s_size, Px_data, Px_size, Py_data, Py_size);
  o_size_idx_0 = Px_size[0] + Py_size[0];
  nx = Px_size[0];
  if (0 <= nx - 1) {
    std::memcpy(&o_data[0], &Px_data[0], nx * sizeof(double));
  }

  nx = Py_size[0];
  for (i = 0; i < nx; i++) {
    o_data[i + Px_size[0]] = Py_data[i];
  }

  obj_fit_size_idx_0 = reqPx_size[0] + reqPy_size[0];
  nx = reqPx_size[0];
  if (0 <= nx - 1) {
    std::memcpy(&obj_fit_data[0], &reqPx_data[0], nx * sizeof(double));
  }

  nx = reqPy_size[0];
  for (i = 0; i < nx; i++) {
    obj_fit_data[i + reqPx_size[0]] = reqPy_data[i];
  }

  penalty = 0.0;
  for (i = 0; i < obj_fit_size_idx_0; i++) {
    p = obj_fit_data[i] - o_data[i];
    penalty += p * p;
  }

  pressure_derivative_jacobian(M_data, M_size, Mx_data, Mx_size, My_data,
    My_size, c_data, s_data, Jx_data, Jx_size, Jy_data, Jy_size);
  if ((Jx_size[0] != 0) && (Jx_size[1] != 0)) {
    u0 = Jx_size[1];
  } else if ((Jy_size[0] != 0) && (Jy_size[1] != 0)) {
    u0 = Jy_size[1];
  } else {
    u0 = Jx_size[1];
    if (u0 <= 0) {
      u0 = 0;
    }

    if (Jy_size[1] > u0) {
      u0 = Jy_size[1];
    }
  }

  empty_non_axis_sizes = (u0 == 0);
  if (empty_non_axis_sizes || ((Jx_size[0] != 0) && (Jx_size[1] != 0))) {
    input_sizes_idx_0 = static_cast<signed char>(Jx_size[0]);
  } else {
    input_sizes_idx_0 = 0;
  }

  if (empty_non_axis_sizes || ((Jy_size[0] != 0) && (Jy_size[1] != 0))) {
    sizes_idx_0 = static_cast<signed char>(Jy_size[0]);
  } else {
    sizes_idx_0 = 0;
  }

  nA = input_sizes_idx_0;
  nx = sizes_idx_0;
  J_size_idx_0 = input_sizes_idx_0 + sizes_idx_0;
  for (i = 0; i < u0; i++) {
    for (i1 = 0; i1 < nA; i1++) {
      J_data[i1 + J_size_idx_0 * i] = Jx_data[i1 + input_sizes_idx_0 * i];
    }
  }

  for (i = 0; i < u0; i++) {
    for (i1 = 0; i1 < nx; i1++) {
      J_data[(i1 + input_sizes_idx_0) + J_size_idx_0 * i] = Jy_data[i1 +
        sizes_idx_0 * i];
    }
  }

  //     %% Levenberg-Marquardt solver
  xr = 1.0;

  //  initial damping
  //      fprintf('|Iteration|Penalty |Damping |LSCount|\n');
  //      fprintf('|---------|--------|--------|-------|\n');
  b_i = 0;
  exitg1 = false;
  while ((!exitg1) && (b_i < 7)) {
    int num_search;

    //  number of iterations
    for (i = 0; i < o_size_idx_0; i++) {
      rhs_data[i] = o_data[i] - obj_fit_data[i];
    }

    num_search = 1;
    int exitg2;
    do {
      short b_input_sizes_idx_0;
      exitg2 = 0;
      p = std::sqrt(xr);
      minszA = M_size_tmp * M_size_tmp;
      sizes_idx_0 = static_cast<signed char>(M_size_tmp);
      for (i = 0; i < J_size_idx_0; i++) {
        for (i1 = 0; i1 < u0; i1++) {
          varargin_1_tmp_data[i1 + u0 * i] = J_data[i + J_size_idx_0 * i1];
        }
      }

      if (0 <= minszA - 1) {
        std::memset(&I_data[0], 0, minszA * sizeof(double));
      }

      if (M_size_tmp > 0) {
        for (b_nA = 0; b_nA < M_size_tmp; b_nA++) {
          I_data[b_nA + M_size_tmp * b_nA] = 1.0;
        }
      }

      nx = M_size_tmp * M_size_tmp;
      for (i = 0; i < nx; i++) {
        varargin_2_data[i] = p * I_data[i];
      }

      if ((u0 != 0) && (J_size_idx_0 != 0)) {
        minszA = J_size_idx_0;
      } else if (M_size_tmp != 0) {
        minszA = sizes_idx_0;
      } else {
        if (J_size_idx_0 > 0) {
          minszA = J_size_idx_0;
        } else {
          minszA = 0;
        }

        if (sizes_idx_0 > minszA) {
          minszA = sizes_idx_0;
        }
      }

      empty_non_axis_sizes = (minszA == 0);
      if (empty_non_axis_sizes || ((u0 != 0) && (J_size_idx_0 != 0))) {
        b_input_sizes_idx_0 = static_cast<short>(u0);
      } else {
        b_input_sizes_idx_0 = 0;
      }

      if (empty_non_axis_sizes || (M_size_tmp != 0)) {
        input_sizes_idx_0 = static_cast<signed char>(M_size_tmp);
      } else {
        input_sizes_idx_0 = 0;
      }

      //              [~, R] = qr(Abar, 0);
      nA = b_input_sizes_idx_0;
      nx = input_sizes_idx_0;
      R_size[0] = b_input_sizes_idx_0 + input_sizes_idx_0;
      R_size[1] = minszA;
      for (i = 0; i < minszA; i++) {
        for (i1 = 0; i1 < nA; i1++) {
          R_data[i1 + R_size[0] * i] = varargin_1_tmp_data[i1 +
            b_input_sizes_idx_0 * i];
        }
      }

      for (i = 0; i < minszA; i++) {
        for (i1 = 0; i1 < nx; i1++) {
          R_data[(i1 + b_input_sizes_idx_0) + R_size[0] * i] =
            varargin_2_data[i1 + input_sizes_idx_0 * i];
        }
      }

      qr(R_data, R_size);
      if (1 > M_size_tmp) {
        nA = 0;
        b_nA = 0;
      } else {
        nA = M_size_tmp;
        b_nA = M_size_tmp;
      }

      for (i = 0; i < b_nA; i++) {
        for (i1 = 0; i1 < nA; i1++) {
          b_R_data[i1 + nA * i] = R_data[i1 + R_size[0] * i];
        }
      }

      R_size[0] = nA;
      R_size[1] = b_nA;
      nx = nA * b_nA;
      if (0 <= nx - 1) {
        std::memcpy(&R_data[0], &b_R_data[0], nx * sizeof(double));
      }

      //              step = -J'*((R'*R)\rhs);
      for (i = 0; i < nA; i++) {
        for (i1 = 0; i1 < b_nA; i1++) {
          b_R_data[i1 + b_nA * i] = R_data[i + nA * i1];
        }
      }

      I_size[0] = b_nA;
      I_size[1] = nA;
      nx = b_nA * nA;
      if (0 <= nx - 1) {
        std::memcpy(&I_data[0], &b_R_data[0], nx * sizeof(double));
      }

      if (b_nA < nA) {
        minszA = b_nA;
      } else {
        minszA = nA;
      }

      new_o_size[0] = nA;
      if (0 <= minszA - 1) {
        std::memcpy(&new_o_data[0], &rhs_data[0], minszA * sizeof(double));
      }

      i = minszA + 1;
      if (i <= nA) {
        std::memset(&new_o_data[i + -1], 0, ((nA - i) + 1) * sizeof(double));
      }

      trisolve(I_data, I_size, new_o_data, new_o_size);
      if (nA >= b_nA) {
        nA = b_nA;
      }

      C_size[0] = b_nA;
      if (0 <= nA - 1) {
        std::memcpy(&C_data[0], &new_o_data[0], nA * sizeof(double));
      }

      i = nA + 1;
      if (i <= b_nA) {
        std::memset(&C_data[i + -1], 0, ((b_nA - i) + 1) * sizeof(double));
      }

      b_trisolve(R_data, R_size, C_data, C_size);
      nx = u0 * J_size_idx_0;
      for (i = 0; i < nx; i++) {
        varargin_1_tmp_data[i] = -varargin_1_tmp_data[i];
      }

      nA = u0 - 1;
      if (0 <= nA) {
        std::memset(&c_data[0], 0, (nA + 1) * sizeof(double));
      }

      for (b_nA = 0; b_nA < J_size_idx_0; b_nA++) {
        nx = b_nA * u0;
        for (minszA = 0; minszA <= nA; minszA++) {
          c_data[minszA] += varargin_1_tmp_data[nx + minszA] * C_data[b_nA];
        }
      }

      c_size[0] = phases_size[0];
      nx = phases_size[0];
      for (i = 0; i < nx; i++) {
        c_data[i] += phases_data[i];
      }

      s_size[0] = phases_size[0];
      nx = phases_size[0];
      if (0 <= nx - 1) {
        std::memcpy(&s_data[0], &c_data[0], nx * sizeof(double));
      }

      nx = phases_size[0];
      for (b_nA = 0; b_nA < nx; b_nA++) {
        s_data[b_nA] = std::cos(s_data[b_nA]);
      }

      new_s_size[0] = phases_size[0];
      nx = phases_size[0];
      if (0 <= nx - 1) {
        std::memcpy(&new_s_data[0], &c_data[0], nx * sizeof(double));
      }

      nx = phases_size[0];
      for (b_nA = 0; b_nA < nx; b_nA++) {
        new_s_data[b_nA] = std::sin(new_s_data[b_nA]);
      }

      absolute_pressure_derivative(M_data, M_size, Mx_data, Mx_size, My_data,
        My_size, s_data, s_size, new_s_data, new_s_size, Px_data, Px_size,
        Py_data, Py_size);
      nA = Px_size[0] + Py_size[0];
      nx = Px_size[0];
      if (0 <= nx - 1) {
        std::memcpy(&new_o_data[0], &Px_data[0], nx * sizeof(double));
      }

      nx = Py_size[0];
      for (i = 0; i < nx; i++) {
        new_o_data[i + Px_size[0]] = Py_data[i];
      }

      phase_shift = 0.0;
      for (i = 0; i < obj_fit_size_idx_0; i++) {
        p = obj_fit_data[i] - new_o_data[i];
        phase_shift += p * p;
      }

      if (phase_shift < penalty) {
        nx = c_size[0];
        if (0 <= nx - 1) {
          std::memcpy(&phases_data[0], &c_data[0], nx * sizeof(double));
        }

        o_size_idx_0 = nA;
        if (0 <= nA - 1) {
          std::memcpy(&o_data[0], &new_o_data[0], nA * sizeof(double));
        }

        penalty = phase_shift;

        //                  fprintf('|%9d|%8.2e|%8.2e|%7d|\n',i,penalty,mu,num_search); 
        pressure_derivative_jacobian(M_data, M_size, Mx_data, Mx_size, My_data,
          My_size, s_data, new_s_data, Jx_data, Jx_size, Jy_data, Jy_size);
        if ((Jx_size[0] != 0) && (Jx_size[1] != 0)) {
          u0 = Jx_size[1];
        } else if ((Jy_size[0] != 0) && (Jy_size[1] != 0)) {
          u0 = Jy_size[1];
        } else {
          u0 = Jx_size[1];
          if (u0 <= 0) {
            u0 = 0;
          }

          if (Jy_size[1] > u0) {
            u0 = Jy_size[1];
          }
        }

        empty_non_axis_sizes = (u0 == 0);
        if (empty_non_axis_sizes || ((Jx_size[0] != 0) && (Jx_size[1] != 0))) {
          input_sizes_idx_0 = static_cast<signed char>(Jx_size[0]);
        } else {
          input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((Jy_size[0] != 0) && (Jy_size[1] != 0))) {
          sizes_idx_0 = static_cast<signed char>(Jy_size[0]);
        } else {
          sizes_idx_0 = 0;
        }

        nA = input_sizes_idx_0;
        nx = sizes_idx_0;
        J_size_idx_0 = input_sizes_idx_0 + sizes_idx_0;
        for (i = 0; i < u0; i++) {
          for (i1 = 0; i1 < nA; i1++) {
            J_data[i1 + J_size_idx_0 * i] = Jx_data[i1 + input_sizes_idx_0 * i];
          }
        }

        for (i = 0; i < u0; i++) {
          for (i1 = 0; i1 < nx; i1++) {
            J_data[(i1 + input_sizes_idx_0) + J_size_idx_0 * i] = Jy_data[i1 +
              sizes_idx_0 * i];
          }
        }

        xr /= 2.0;
        exitg2 = 1;
      } else {
        xr *= 2.0;
        num_search = static_cast<int>((num_search + 1U));
        if (num_search > 10) {
          //                  fprintf('LMsolve failed to converge on iteration %u\n',i); 
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    if (penalty < 1.0) {
      //  penalty tolerance
      exitg1 = true;
    } else {
      b_i++;
    }
  }
}

// End of code generation (LM_solve_gradient.cpp)
