//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  interp1.cpp
//
//  Code generation for function 'interp1'
//


// Include files
#include "interp1.h"
#include "LM_solve_gradient.h"
#include "rt_nonfinite.h"
#include <cstring>

// Function Definitions
double interp1(const double varargin_1[1001], const double varargin_2[1001],
               double varargin_3)
{
  double Vq;
  double y[1001];
  double x[1001];
  int low_i;
  std::memcpy(&y[0], &varargin_2[0], 1001U * sizeof(double));
  std::memcpy(&x[0], &varargin_1[0], 1001U * sizeof(double));
  low_i = 0;
  int exitg1;
  do {
    exitg1 = 0;
    if (low_i < 1001) {
      if (rtIsNaN(varargin_1[low_i])) {
        exitg1 = 1;
      } else {
        low_i++;
      }
    } else {
      double xtmp;
      if (varargin_1[1] < varargin_1[0]) {
        for (low_i = 0; low_i < 500; low_i++) {
          xtmp = x[low_i];
          x[low_i] = x[1000 - low_i];
          x[1000 - low_i] = xtmp;
          xtmp = y[low_i];
          y[low_i] = y[1000 - low_i];
          y[1000 - low_i] = xtmp;
        }
      }

      Vq = rtNaN;
      if ((!rtIsNaN(varargin_3)) && (!(varargin_3 > x[1000])) && (!(varargin_3 <
            x[0]))) {
        int low_ip1;
        int high_i;
        low_i = 1;
        low_ip1 = 2;
        high_i = 1001;
        while (high_i > low_ip1) {
          int mid_i;
          mid_i = (low_i + high_i) >> 1;
          if (varargin_3 >= x[mid_i - 1]) {
            low_i = mid_i;
            low_ip1 = mid_i + 1;
          } else {
            high_i = mid_i;
          }
        }

        xtmp = x[low_i - 1];
        xtmp = (varargin_3 - xtmp) / (x[low_i] - xtmp);
        if (xtmp == 0.0) {
          Vq = y[low_i - 1];
        } else if (xtmp == 1.0) {
          Vq = y[low_i];
        } else if (y[low_i - 1] == y[low_i]) {
          Vq = y[low_i - 1];
        } else {
          Vq = (1.0 - xtmp) * y[low_i - 1] + xtmp * y[low_i];
        }
      }

      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return Vq;
}

// End of code generation (interp1.cpp)
