//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  p.cpp
//
//  Code generation for function 'p'
//


// Include files
#include "p.h"
#include "LM_solve_pressure.h"
#include "generate_lookup_data.h"
#include "rt_nonfinite.h"
#include <cmath>
#include <cstring>

// Function Definitions
void p(const double x_data[], const int x_size[1], const double y_data[], const
       double z_data[], const double tx_data[], const int tx_size[1], const
       double ty_data[], double M_data[], int M_size[2])
{
  int low_i;
  int i;
  double y[1001];
  double x[1001];
  double Vq;

  //  transducer power [Pa.m]
  //  wavenumber [m^{-1}]
  //      r = 0.005; % transducer radius [m]
  M_size[0] = tx_size[0];
  low_i = x_size[0] << 1;
  M_size[1] = low_i;
  low_i *= tx_size[0];
  if (0 <= low_i - 1) {
    std::memset(&M_data[0], 0, low_i * sizeof(double));
  }

  i = x_size[0];
  for (int b_i = 0; b_i < i; b_i++) {
    int i1;

    //  iterate over points
    i1 = tx_size[0];
    for (int j = 0; j < i1; j++) {
      double xr;
      double yr;
      double d;

      //  iterate over transducers
      xr = x_data[b_i] - tx_data[j];
      yr = y_data[b_i] - ty_data[j];
      xr = xr * xr + yr * yr;
      d = std::sqrt(xr + z_data[b_i] * z_data[b_i]);

      //              lookup_index = (N-1)*sqrt(xr^2 + yr^2)/d;
      //              index_rounded = floor(lookup_index);
      yr = std::sqrt(xr) / d;
      std::memcpy(&y[0], &directivity[0], 1001U * sizeof(double));
      std::memcpy(&x[0], &sinAngle[0], 1001U * sizeof(double));
      low_i = 0;
      int exitg1;
      do {
        exitg1 = 0;
        if (low_i < 1001) {
          if (rtIsNaN(sinAngle[low_i])) {
            exitg1 = 1;
          } else {
            low_i++;
          }
        } else {
          if (sinAngle[1] < sinAngle[0]) {
            for (low_i = 0; low_i < 500; low_i++) {
              xr = x[low_i];
              x[low_i] = x[1000 - low_i];
              x[1000 - low_i] = xr;
              xr = y[low_i];
              y[low_i] = y[1000 - low_i];
              y[1000 - low_i] = xr;
            }
          }

          Vq = rtNaN;
          if ((!rtIsNaN(yr)) && (!(yr > x[1000])) && (!(yr < x[0]))) {
            int low_ip1;
            int high_i;
            low_i = 1;
            low_ip1 = 2;
            high_i = 1001;
            while (high_i > low_ip1) {
              int mid_i;
              mid_i = (low_i + high_i) >> 1;
              if (yr >= x[mid_i - 1]) {
                low_i = mid_i;
                low_ip1 = mid_i + 1;
              } else {
                high_i = mid_i;
              }
            }

            xr = x[low_i - 1];
            xr = (yr - xr) / (x[low_i] - xr);
            if (xr == 0.0) {
              Vq = y[low_i - 1];
            } else if (xr == 1.0) {
              Vq = y[low_i];
            } else if (y[low_i - 1] == y[low_i]) {
              Vq = y[low_i - 1];
            } else {
              Vq = (1.0 - xr) * y[low_i - 1] + xr * y[low_i];
            }
          }

          exitg1 = 1;
        }
      } while (exitg1 == 0);

      yr = 5.0 * Vq / d;
      xr = 732.733 * d;
      low_i = (b_i + 1) << 1;
      M_data[j + M_size[0] * (low_i - 2)] = yr * std::cos(xr);
      M_data[j + M_size[0] * (low_i - 1)] = yr * std::sin(xr);
    }
  }
}

// End of code generation (p.cpp)
