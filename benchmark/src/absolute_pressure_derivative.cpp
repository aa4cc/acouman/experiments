//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_derivative.cpp
//
//  Code generation for function 'absolute_pressure_derivative'
//


// Include files
#include "absolute_pressure_derivative.h"
#include "LM_solve_gradient.h"
#include "rtwutil.h"
#include "pressure_derivative_jacobian.h"
#include "rt_nonfinite.h"

// Function Definitions
void absolute_pressure_derivative(const double M_data[], const int M_size[2],
  const double Mx_data[], const int Mx_size[2], const double My_data[], const
  int My_size[2], const double c_data[], const int c_size[1], const double
  s_data[], const int s_size[1], double abs_p_x_data[], int abs_p_x_size[1],
  double abs_p_y_data[], int abs_p_y_size[1])
{
  int num_pp;
  int varargin_1_size_idx_0;
  int b_varargin_1_size_idx_0;
  int c_varargin_1_size_idx_0;
  int loop_ub;
  int b_loop_ub;
  int c_loop_ub;
  double p_data[512];
  int d_loop_ub;
  int e_loop_ub;
  double px_data[512];
  int f_loop_ub;
  int g_loop_ub;
  double y_tmp_idx_0;
  double py_data[512];
  int h_loop_ub;
  double y_tmp_idx_1;
  double b_y_tmp_idx_0;
  double b_y_tmp_idx_1;
  double c;
  double varargin_1_data[256];
  double C_data[256];
  double b_C_data[256];
  double c_C_data[256];
  double s;
  double b_c;
  double b_s;
  num_pp = static_cast<int>(static_cast<unsigned int>(rt_roundd_snf(static_cast<
    double>(M_size[1]) / 2.0)));
  abs_p_x_size[0] = num_pp;
  abs_p_y_size[0] = num_pp;
  if (0 <= num_pp - 1) {
    varargin_1_size_idx_0 = M_size[0];
    b_varargin_1_size_idx_0 = Mx_size[0];
    c_varargin_1_size_idx_0 = My_size[0];
    loop_ub = c_size[0];
    b_loop_ub = s_size[0];
    c_loop_ub = c_size[0];
    d_loop_ub = s_size[0];
    e_loop_ub = c_size[0];
    f_loop_ub = s_size[0];
    g_loop_ub = c_size[0];
    h_loop_ub = s_size[0];
  }

  for (int i = 0; i < num_pp; i++) {
    int k;
    int b_i;
    double B_idx_0_tmp;
    double B_idx_1_tmp;
    double b_B_idx_0_tmp;
    double b_B_idx_1_tmp;
    k = static_cast<int>(((i + 1U) << 1));
    for (b_i = 0; b_i < varargin_1_size_idx_0; b_i++) {
      p_data[b_i] = M_data[b_i + M_size[0] * (k - 2)];
      p_data[b_i + varargin_1_size_idx_0] = M_data[b_i + M_size[0] * (k - 1)];
    }

    for (b_i = 0; b_i < b_varargin_1_size_idx_0; b_i++) {
      px_data[b_i] = Mx_data[b_i + Mx_size[0] * (k - 2)];
      px_data[b_i + b_varargin_1_size_idx_0] = Mx_data[b_i + Mx_size[0] * (k - 1)];
    }

    for (b_i = 0; b_i < c_varargin_1_size_idx_0; b_i++) {
      py_data[b_i] = My_data[b_i + My_size[0] * (k - 2)];
      py_data[b_i + c_varargin_1_size_idx_0] = My_data[b_i + My_size[0] * (k - 1)];
    }

    y_tmp_idx_0 = 0.0;
    y_tmp_idx_1 = 0.0;
    b_y_tmp_idx_0 = 0.0;
    b_y_tmp_idx_1 = 0.0;
    for (k = 0; k < varargin_1_size_idx_0; k++) {
      y_tmp_idx_0 += p_data[k] * c_data[k];
      c = p_data[varargin_1_size_idx_0 + k];
      y_tmp_idx_1 += c * c_data[k];
      b_y_tmp_idx_0 += p_data[k] * s_data[k];
      b_y_tmp_idx_1 += c * s_data[k];
    }

    B_idx_0_tmp = 0.0 * b_y_tmp_idx_0 + -b_y_tmp_idx_1;
    B_idx_1_tmp = b_y_tmp_idx_0 + 0.0 * b_y_tmp_idx_1;
    for (k = 0; k < b_varargin_1_size_idx_0; k++) {
      c = px_data[b_varargin_1_size_idx_0 + k];
      varargin_1_data[k] = px_data[k] * y_tmp_idx_0 + c * y_tmp_idx_1;
      C_data[k] = px_data[k] * b_y_tmp_idx_0 + c * b_y_tmp_idx_1;
      b_C_data[k] = px_data[k] * B_idx_0_tmp + c * B_idx_1_tmp;
    }

    b_B_idx_0_tmp = 0.0 * y_tmp_idx_0 + -y_tmp_idx_1;
    b_B_idx_1_tmp = y_tmp_idx_0 + 0.0 * y_tmp_idx_1;
    for (k = 0; k < b_varargin_1_size_idx_0; k++) {
      c_C_data[k] = px_data[k] * b_B_idx_0_tmp + px_data[b_varargin_1_size_idx_0
        + k] * b_B_idx_1_tmp;
    }

    c = 0.0;
    for (k = 0; k < loop_ub; k++) {
      c += c_data[k] * varargin_1_data[k];
    }

    s = 0.0;
    for (k = 0; k < b_loop_ub; k++) {
      s += s_data[k] * C_data[k];
    }

    b_c = 0.0;
    for (k = 0; k < c_loop_ub; k++) {
      b_c += c_data[k] * b_C_data[k];
    }

    b_s = 0.0;
    for (k = 0; k < d_loop_ub; k++) {
      b_s += s_data[k] * c_C_data[k];
    }

    abs_p_x_data[i] = 2.0 * (((c + s) + b_c) - b_s);
    for (k = 0; k < c_varargin_1_size_idx_0; k++) {
      c = py_data[c_varargin_1_size_idx_0 + k];
      varargin_1_data[k] = py_data[k] * y_tmp_idx_0 + c * y_tmp_idx_1;
      C_data[k] = py_data[k] * b_y_tmp_idx_0 + c * b_y_tmp_idx_1;
      b_C_data[k] = py_data[k] * B_idx_0_tmp + c * B_idx_1_tmp;
    }

    for (k = 0; k < c_varargin_1_size_idx_0; k++) {
      c_C_data[k] = py_data[k] * b_B_idx_0_tmp + py_data[c_varargin_1_size_idx_0
        + k] * b_B_idx_1_tmp;
    }

    c = 0.0;
    for (k = 0; k < e_loop_ub; k++) {
      c += c_data[k] * varargin_1_data[k];
    }

    s = 0.0;
    for (k = 0; k < f_loop_ub; k++) {
      s += s_data[k] * C_data[k];
    }

    b_c = 0.0;
    for (k = 0; k < g_loop_ub; k++) {
      b_c += c_data[k] * b_C_data[k];
    }

    b_s = 0.0;
    for (k = 0; k < h_loop_ub; k++) {
      b_s += s_data[k] * c_C_data[k];
    }

    abs_p_y_data[i] = 2.0 * (((c + s) + b_c) - b_s);
  }
}

// End of code generation (absolute_pressure_derivative.cpp)
