//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  pressure_jacobian.cpp
//
//  Code generation for function 'pressure_jacobian'
//


// Include files
#include "pressure_jacobian.h"
#include "LM_solve_pressure.h"
#include "rtwutil.h"
#include "rt_nonfinite.h"
#include <cstring>

// Function Definitions
void pressure_jacobian(const double M_data[], const int M_size[2], const double
  c_data[], const int c_size[1], const double s_data[], const int s_size[1],
  double J_data[], int J_size[2])
{
  int num_pp;
  int loop_ub;
  double Mi1_data[256];
  double Mi1;
  double tmp_data[256];
  double M;
  double b_Mi1;
  double a_tmp_data[256];
  double b_M;
  num_pp = static_cast<int>(static_cast<unsigned int>(rt_roundd_snf(static_cast<
    double>(M_size[1]) / 2.0)));
  J_size[0] = num_pp;
  J_size[1] = M_size[0];
  loop_ub = num_pp * M_size[0];
  if (0 <= loop_ub - 1) {
    std::memset(&J_data[0], 0, loop_ub * sizeof(double));
  }

  for (int i = 0; i < num_pp; i++) {
    int Mi1_tmp;
    int b_i;
    int b_loop_ub;

    //          Mi = [M(:,2*i-1), M(:,2*i)];
    //          Ji = 2*(diag(c)*Mi*[0,1;-1,0] - diag(s)*Mi)*Mi'*c + 2*(diag(c)*Mi+diag(s)*Mi*[0,1;-1,0])*Mi'*s; 
    //          Mi = [M(:,2*i-1), M(:,2*i)];
    //          Mic = [c.*M(:,2*i-1), c.*M(:,2*i)];
    //          Mis = [s.*M(:,2*i-1), s.*M(:,2*i)];
    //
    //          Ji = 2*(Mic*[0,1;-1,0] - Mis)*(Mi'*c) + 2*(Mic+Mis*[0,1;-1,0])*(Mi'*s); 
    Mi1_tmp = static_cast<int>(((i + 1U) << 1)) - 1;
    loop_ub = M_size[0];
    for (b_i = 0; b_i < loop_ub; b_i++) {
      Mi1_data[b_i] = M_data[b_i + M_size[0] * (Mi1_tmp - 1)];
    }

    //          Ji = 2*[-c.*Mi2-s.*Mi1, c.*Mi1-s.*Mi2]*([Mi1';Mi2']*c) + 2*([c.*Mi1-s.*Mi2, c.*Mi2+s.*Mi1])*([Mi1';Mi2']*s); 
    b_loop_ub = s_size[0];
    for (b_i = 0; b_i < b_loop_ub; b_i++) {
      tmp_data[b_i] = s_data[b_i] * Mi1_data[b_i];
    }

    Mi1 = 0.0;
    M = 0.0;
    for (b_i = 0; b_i < loop_ub; b_i++) {
      Mi1 += Mi1_data[b_i] * c_data[b_i];
      M += M_data[b_i + M_size[0] * Mi1_tmp] * c_data[b_i];
    }

    b_loop_ub = c_size[0];
    for (b_i = 0; b_i < b_loop_ub; b_i++) {
      a_tmp_data[b_i] = c_data[b_i] * Mi1_data[b_i] - s_data[b_i] * M_data[b_i +
        M_size[0] * Mi1_tmp];
    }

    b_Mi1 = 0.0;
    b_M = 0.0;
    for (b_i = 0; b_i < loop_ub; b_i++) {
      b_Mi1 += Mi1_data[b_i] * s_data[b_i];
      b_M += M_data[b_i + M_size[0] * Mi1_tmp] * s_data[b_i];
    }

    loop_ub = J_size[1];
    for (b_i = 0; b_i < loop_ub; b_i++) {
      b_loop_ub = b_i + M_size[0] * Mi1_tmp;
      J_data[i + num_pp * b_i] = 2.0 * ((-c_data[b_i] * M_data[b_loop_ub] -
        tmp_data[b_i]) * Mi1 + a_tmp_data[b_i] * M) + 2.0 * (a_tmp_data[b_i] *
        b_Mi1 + (c_data[b_i] * M_data[b_loop_ub] + tmp_data[b_i]) * b_M);
    }
  }
}

// End of code generation (pressure_jacobian.cpp)
