//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  trisolve.cpp
//
//  Code generation for function 'trisolve'
//


// Include files
#include "trisolve.h"
#include "LM_solve_gradient.h"
#include "rt_nonfinite.h"

// Function Definitions
void b_trisolve(const double A_data[], const int A_size[2], double B_data[],
                const int B_size[1])
{
  int kAcol;
  int u1;
  int b_u1;
  kAcol = A_size[0];
  u1 = A_size[1];
  if (kAcol < u1) {
    u1 = kAcol;
  }

  b_u1 = B_size[0];
  if (u1 < b_u1) {
    b_u1 = u1;
  }

  if (B_size[0] != 0) {
    for (u1 = b_u1; u1 >= 1; u1--) {
      double d;
      kAcol = A_size[0] * (u1 - 1);
      d = B_data[u1 - 1];
      if (d != 0.0) {
        B_data[u1 - 1] = d / A_data[(u1 + kAcol) - 1];
        for (int i = 0; i <= u1 - 2; i++) {
          B_data[i] -= B_data[u1 - 1] * A_data[i + kAcol];
        }
      }
    }
  }
}

void trisolve(const double A_data[], const int A_size[2], double B_data[], const
              int B_size[1])
{
  int kAcol;
  int u1;
  int b_u1;
  kAcol = A_size[0];
  u1 = A_size[1];
  if (kAcol < u1) {
    u1 = kAcol;
  }

  b_u1 = B_size[0];
  if (u1 < b_u1) {
    b_u1 = u1;
  }

  if (B_size[0] != 0) {
    for (int k = 0; k < b_u1; k++) {
      kAcol = A_size[0] * k;
      if (B_data[k] != 0.0) {
        B_data[k] /= A_data[k + kAcol];
        u1 = k + 2;
        for (int i = u1; i <= b_u1; i++) {
          B_data[i - 1] -= B_data[k] * A_data[(i + kAcol) - 1];
        }
      }
    }
  }
}

// End of code generation (trisolve.cpp)
