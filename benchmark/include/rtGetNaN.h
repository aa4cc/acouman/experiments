//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  rtGetNaN.h
//
//  Code generation for function 'LM_solve_gradient'
//


#ifndef RTGETNAN_H
#define RTGETNAN_H
#include "rtwtypes.h"
#include "rt_nonfinite.h"
#ifndef __cplusplus
#include <stddef.h>
#else
#include <cstddef>

extern "C" {

#endif

  extern real_T rtGetNaN(void);
  extern real32_T rtGetNaNF(void);

#ifdef __cplusplus

}
#endif
#endif

// End of code generation (rtGetNaN.h)
