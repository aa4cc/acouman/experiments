//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_derivative_wrapper_data.h
//
//  Code generation for function 'absolute_pressure_derivative_wrapper_data'
//


#ifndef GENERATE_LOOKUP_DATA_H
#define GENERATE_LOOKUP_DATA_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Variable Declarations
extern double sinAngle[1001];
extern double directivity[1001];
extern double directivity_derivative[1001];
extern bool isInitialized_lookup;

#endif

// End of code generation (absolute_pressure_derivative_wrapper_data.h)
