//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_wrapper.h
//
//  Code generation for function 'absolute_pressure_wrapper'
//


#ifndef ABSOLUTE_PRESSURE_WRAPPER_H
#define ABSOLUTE_PRESSURE_WRAPPER_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void absolute_pressure_wrapper(const double x_data[], const int x_size[1],
  const double y_data[], const int y_size[1], const double z_data[], const int
  z_size[1], const double tx_data[], const int tx_size[1], const double ty_data[],
  const int ty_size[1], const double phases_data[], const int phases_size[1],
  double P_data[], int P_size[1]);

#endif

// End of code generation (absolute_pressure_wrapper.h)
