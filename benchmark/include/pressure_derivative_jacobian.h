//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  pressure_derivative_jacobian.h
//
//  Code generation for function 'pressure_derivative_jacobian'
//


#ifndef PRESSURE_DERIVATIVE_JACOBIAN_H
#define PRESSURE_DERIVATIVE_JACOBIAN_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void pressure_derivative_jacobian(const double M_data[], const int
  M_size[2], const double Mx_data[], const int Mx_size[2], const double My_data[],
  const int My_size[2], const double c_data[], const double s_data[], double
  Jx_data[], int Jx_size[2], double Jy_data[], int Jy_size[2]);

#endif

// End of code generation (pressure_derivative_jacobian.h)
