//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  p.h
//
//  Code generation for function 'p'
//


#ifndef P_H
#define P_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void p(const double x_data[], const int x_size[1], const double y_data[],
              const double z_data[], const double tx_data[], const int tx_size[1],
              const double ty_data[], double M_data[], int M_size[2]);

#endif

// End of code generation (p.h)
