//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_derivative.h
//
//  Code generation for function 'absolute_pressure_derivative'
//


#ifndef ABSOLUTE_PRESSURE_DERIVATIVE_H
#define ABSOLUTE_PRESSURE_DERIVATIVE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void absolute_pressure_derivative(const double M_data[], const int
  M_size[2], const double Mx_data[], const int Mx_size[2], const double My_data[],
  const int My_size[2], const double c_data[], const int c_size[1], const double
  s_data[], const int s_size[1], double abs_p_x_data[], int abs_p_x_size[1],
  double abs_p_y_data[], int abs_p_y_size[1]);

#endif

// End of code generation (absolute_pressure_derivative.h)
