//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure.h
//
//  Code generation for function 'absolute_pressure'
//


#ifndef ABSOLUTE_PRESSURE_H
#define ABSOLUTE_PRESSURE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void absolute_pressure(const double M_data[], const int M_size[2], const
  double c_data[], const int c_size[1], const double s_data[], const int s_size
  [1], double abs_p_data[], int abs_p_size[1]);

#endif

// End of code generation (absolute_pressure.h)
