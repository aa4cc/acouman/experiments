//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  qr.h
//
//  Code generation for function 'qr'
//


#ifndef QR_H
#define QR_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "coder_array.h"

// Function Declarations
extern void qr(double A_data[], const int A_size[2]);

extern void qr(coder::array<double, 2U> &A);

#endif

// End of code generation (qr.h)
