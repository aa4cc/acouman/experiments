//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  trisolve.h
//
//  Code generation for function 'trisolve'
//


#ifndef TRISOLVE_H
#define TRISOLVE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void b_trisolve(const double A_data[], const int A_size[2], double
  B_data[], const int B_size[1]);
extern void trisolve(const double A_data[], const int A_size[2], double B_data[],
                     const int B_size[1]);

#endif

// End of code generation (trisolve.h)
