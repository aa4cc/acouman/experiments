//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  xzgeqp3.h
//
//  Code generation for function 'xzgeqp3'
//


#ifndef XZGEQP3_H
#define XZGEQP3_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "coder_array.h"

// Function Declarations
extern void qrf(double A_data[], const int A_size[2], int m, int n, int nfxd,
                double tau_data[]);

extern void qrf(coder::array<double, 2U> &A, int m, int n, int nfxd, double
                tau_data[]);

#endif

// End of code generation (xzgeqp3.h)
