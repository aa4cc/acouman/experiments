//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  generate_lookup.h
//
//  Code generation for function 'generate_lookup'
//


#ifndef GENERATE_LOOKUP_H
#define GENERATE_LOOKUP_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void generate_lookup();

#endif

// End of code generation (generate_lookup.h)
