//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  LM_solve_gradient.h
//
//  Code generation for function 'LM_solve_gradient'
//


#ifndef LM_SOLVE_GRADIENT_H
#define LM_SOLVE_GRADIENT_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void LM_solve_gradient(const double x_data[], const int x_size[1], const
  double y_data[], const int y_size[1], const double z_data[], const int z_size
  [1], const double reqPx_data[], const int reqPx_size[1], const double
  reqPy_data[], const int reqPy_size[1], const double tx_data[], const int
  tx_size[1], const double ty_data[], const int ty_size[1], double phases_data[],
  int phases_size[1]);

#endif

// End of code generation (LM_solve_gradient.h)
