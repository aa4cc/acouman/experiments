#ifndef UTILS_H
#define UTILS_H

#include <cstdlib>

void generate_array(int array_size, double *tx, double *ty);

void print_vector(double *x, int len, const char* sep);

#endif