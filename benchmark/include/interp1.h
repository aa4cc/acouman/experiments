//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  interp1.h
//
//  Code generation for function 'interp1'
//


#ifndef INTERP1_H
#define INTERP1_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern double interp1(const double varargin_1[1001], const double varargin_2
                      [1001], double varargin_3);

#endif

// End of code generation (interp1.h)
