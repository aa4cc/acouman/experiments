//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  absolute_pressure_derivative_wrapper_terminate.h
//
//  Code generation for function 'absolute_pressure_derivative_wrapper_terminate'
//


#ifndef TERMINATE_H
#define TERMINATE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "generate_lookup_data.h"

// Function Declarations
extern void lookup_terminate();

#endif

// End of code generation (absolute_pressure_derivative_wrapper_terminate.h)
