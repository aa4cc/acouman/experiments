//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  rtwutil.h
//
//  Code generation for function 'rtwutil'
//


#ifndef RTWUTIL_H
#define RTWUTIL_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern double rt_roundd_snf(double u);

#endif

// End of code generation (rtwutil.h)
