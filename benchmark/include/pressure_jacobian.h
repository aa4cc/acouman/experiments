//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  pressure_jacobian.h
//
//  Code generation for function 'pressure_jacobian'
//


#ifndef PRESSURE_JACOBIAN_H
#define PRESSURE_JACOBIAN_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern void pressure_jacobian(const double M_data[], const int M_size[2], const
  double c_data[], const int c_size[1], const double s_data[], const int s_size
  [1], double J_data[], int J_size[2]);

#endif

// End of code generation (pressure_jacobian.h)
