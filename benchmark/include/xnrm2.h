//
//  Prerelease License - for engineering feedback and testing purposes
//  only. Not for sale.
//
//  xnrm2.h
//
//  Code generation for function 'xnrm2'
//


#ifndef XNRM2_H
#define XNRM2_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "coder_array.h"

// Function Declarations
extern double xnrm2(int n, const double x_data[], int ix0);

extern double xnrm2(int n, const coder::array<double, 2U> &x, int ix0);

#endif

// End of code generation (xnrm2.h)
