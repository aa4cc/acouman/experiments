%% import CSV files
array_sizes = 8:16;
num_arrays = numel(array_sizes);
points = 1:4;
num_points = numel(points);
amplitudes = 1000:250:2500;
num_amplitudes = numel(amplitudes);

BFGS_elapsed = zeros(num_arrays, num_points, num_amplitudes);
BFGS_error = zeros(num_arrays, num_points, num_amplitudes);
LM_pressure_elapsed = zeros(num_arrays, num_points, num_amplitudes);
LM_pressure_error = zeros(num_arrays, num_points, num_amplitudes);
LM_pressure_gradient_elapsed = zeros(num_arrays, num_points, num_amplitudes);
LM_pressure_gradient_error = zeros(num_arrays, num_points, num_amplitudes);
for k = 1:num_arrays
    BFGS_elapsed(k,:,:) = readmatrix(sprintf('BFGS_elapsed_%dx%d.csv',array_sizes(k),array_sizes(k)),'Range','B2:H5');
    BFGS_error(k,:,:) = readmatrix(sprintf('BFGS_error_%dx%d.csv',array_sizes(k),array_sizes(k)),'Range','B2:H5');
    LM_pressure_elapsed(k,:,:) = readmatrix(sprintf('LM_pressure_elapsed_%dx%d.csv',array_sizes(k),array_sizes(k)),'Range','B2:H5');
    LM_pressure_error(k,:,:) = readmatrix(sprintf('LM_pressure_error_%dx%d.csv',array_sizes(k),array_sizes(k)),'Range','B2:H5');
    LM_pressure_gradient_elapsed(k,:,:) = readmatrix(sprintf('LM_pressure_gradient_elapsed_%dx%d.csv',array_sizes(k),array_sizes(k)),'Range','B2:H5');
    LM_pressure_gradient_error(k,:,:) = readmatrix(sprintf('LM_pressure_gradient_error_%dx%d.csv',array_sizes(k),array_sizes(k)),'Range','B2:H5');
end

%% amplitude cross-section
array_idx = 9;
point_idx = 1;

figure(1)
clf
plot(amplitudes, reshape(BFGS_elapsed(array_idx, point_idx, :), 1, num_amplitudes), '--x');
hold on
plot(amplitudes, reshape(LM_pressure_elapsed(array_idx, point_idx, :), 1, num_amplitudes), '--x');
plot(amplitudes, reshape(LM_pressure_gradient_elapsed(array_idx, point_idx, :), 1, num_amplitudes), '--x');
xlabel('Required amplitude [Pa]')
ylabel('Elapsed time [ms]')
title(sprintf('Runtime of algorithms on \n%d-by-%d array with %d point(s)', array_sizes(array_idx), ...
    array_sizes(array_idx), points(point_idx)))
legend('BFGS','LM with specified amplitude',sprintf('LM with specified amplitude\nand zero spatial derivatives'))

figure(2)
clf
semilogy(amplitudes, reshape(BFGS_error(array_idx, point_idx, :), 1, num_amplitudes), '--x');
hold on
semilogy(amplitudes, reshape(LM_pressure_error(array_idx, point_idx, :), 1, num_amplitudes), '--x');
semilogy(amplitudes, reshape(LM_pressure_gradient_error(array_idx, point_idx, :), 1, num_amplitudes), '--x');
xlabel('Required amplitude [Pa]')
ylabel('Mean square error [Pa^2]')
title(sprintf('Error of algorithms on \n%d-by-%d array with %d point(s)', array_sizes(array_idx), ...
    array_sizes(array_idx), points(point_idx)))
legend('BFGS','LM with specified amplitude',sprintf('LM with specified amplitude\nand zero spatial derivatives'))

%% array size cross-section
ampl_idx = 3;
point_idx = 1;

figure(3)
clf
plot(array_sizes, reshape(BFGS_elapsed(:, point_idx, ampl_idx), 1, num_arrays), '--x');
hold on
plot(array_sizes, reshape(LM_pressure_elapsed(:, point_idx, ampl_idx), 1, num_arrays), '--x');
plot(array_sizes, reshape(LM_pressure_gradient_elapsed(:, point_idx, ampl_idx), 1, num_arrays), '--x');
xlabel('Number of transducers in a row')
ylabel('Elapsed time [ms]')
title(sprintf('Runtime of algorithms on \n%d point(s) with required amplitude %.0f Pa',  points(point_idx), ...
    amplitudes(ampl_idx)))
legend('BFGS','LM with specified amplitude',sprintf('LM with specified amplitude\nand zero spatial derivatives'))

figure(4)
clf
semilogy(array_sizes, reshape(BFGS_error(:, point_idx, ampl_idx), 1, num_arrays), '--x');
hold on
semilogy(array_sizes, reshape(LM_pressure_error(:, point_idx, ampl_idx), 1, num_arrays), '--x');
semilogy(array_sizes, reshape(LM_pressure_gradient_error(:, point_idx, ampl_idx), 1, num_arrays), '--x');
xlabel('Number of transducers in a row')
ylabel('Mean square error [Pa^2]')
title(sprintf('Error of algorithms on \n%d point(s) with required amplitude %.0f Pa',  points(point_idx), ...
    amplitudes(ampl_idx)))
legend('BFGS','LM with specified amplitude',sprintf('LM with specified amplitude\nand zero spatial derivatives'))

%% number of points cross-section
ampl_idx = 3;
array_idx = 9;

figure(5)
clf
plot(points, reshape(BFGS_elapsed(array_idx, :, ampl_idx), 1, num_points), '--x');
hold on
plot(points, reshape(LM_pressure_elapsed(array_idx, :, ampl_idx), 1, num_points), '--x');
plot(points, reshape(LM_pressure_gradient_elapsed(array_idx, :, ampl_idx), 1, num_points), '--x');
xlabel('Number of points')
ylabel('Elapsed time [ms]')
title(sprintf('Runtime of algorithms on \n%d-by-%d array with required amplitude %.0f Pa',  array_sizes(array_idx), ...
    array_sizes(array_idx), amplitudes(ampl_idx)))
legend('BFGS','LM with specified amplitude',sprintf('LM with specified amplitude\nand zero spatial derivatives'))

figure(6)
clf
semilogy(points, reshape(BFGS_error(array_idx, :, ampl_idx), 1, num_points), '--x');
hold on
semilogy(points, reshape(LM_pressure_error(array_idx, :, ampl_idx), 1, num_points), '--x');
semilogy(points, reshape(LM_pressure_gradient_error(array_idx, :, ampl_idx), 1, num_points), '--x');
xlabel('Number of points')
ylabel('Mean square error [Pa^2]')
title(sprintf('Error of algorithms on \n%d-by-%d array with required amplitude %.0f Pa',  array_sizes(array_idx), ...
    array_sizes(array_idx), amplitudes(ampl_idx)))
legend('BFGS','LM with specified amplitude',sprintf('LM with specified amplitude\nand zero spatial derivatives'))