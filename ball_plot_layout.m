function [t,xax,yax] = ball_plot_layout(n)
    if nargin < 1
        n = 1;
    end
    figure(n)
    clf
    t = tiledlayout(2,1);
    xax = nexttile;
    hold on
    grid on
    yax = nexttile;
    hold on
    grid on
    ylabel(xax, '$x$ [mm]', 'Interpreter', 'latex')
    xlabel(yax, '$t$ [s]', 'Interpreter', 'latex')
    ylabel(yax, '$y$ [mm]', 'Interpreter', 'latex')
    xax.YAxis.TickLabelInterpreter = 'latex';
    xax.XAxis.TickLabelInterpreter = 'latex';
    yax.YAxis.TickLabelInterpreter = 'latex';
    yax.XAxis.TickLabelInterpreter = 'latex';
    linkaxes([xax,yax],'x');
    t.TileSpacing = 'compact';
end
