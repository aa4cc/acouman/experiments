%% parameters
folder = '06';

ball_size = 500;
est_size = 150;
ctrl_size = 75;
ref_size = 250;

area_size = 70;

playback_speed = 0.5;
%% loading 
load(sprintf('%s/ballpos.mat',folder))
load(sprintf('%s/controller.mat',folder))
load(sprintf('%s/kalman.mat',folder))
load(sprintf('%s/reference.mat',folder))

time = ballpos(1,:);
EstX = kalman([2,4,6],:);
EstY = kalman([3,5,7],:);

CamX = ballpos([2,4,6],:);
CamY = ballpos([3,5,7],:);

ConX = controller([2,3,4],:) * 1e3;
ConY = controller([5,6,7],:) * 1e3;

RefX = ref([2,4,6],:);
RefY = ref([3,5,7],:);

clear ballpos controller kalman ref

%% plot
figure
clf
hold on
balls = [scatter(0,0,ball_size,'ro','filled');
    scatter(0,0,ball_size,'yo','filled');
    scatter(0,0,ball_size,'bo','filled')];
ests = [scatter(0,0,est_size,'o','MarkerFaceColor', 'red', 'MarkerEdgeColor', 'black', 'LineWidth', 1.5);
    scatter(0,0,est_size,'o','MarkerFaceColor', 'yellow', 'MarkerEdgeColor', 'black', 'LineWidth', 1.5);
    scatter(0,0,est_size,'o','MarkerFaceColor', 'blue', 'MarkerEdgeColor', 'black', 'LineWidth', 1.5)];
cons = [scatter(0,0,ctrl_size,'ro','filled');
    scatter(0,0,ctrl_size,'yo','filled');
    scatter(0,0,ctrl_size,'bo','filled')];
refs = [scatter(0,0,ctrl_size,'rx','LineWidth', 1.5);
    scatter(0,0,ctrl_size,'yx','LineWidth', 1.5);
    scatter(0,0,ctrl_size,'bx','LineWidth', 1.5)];
xlim([-area_size,area_size])
ylim([-area_size,area_size])


for k = 1:numel(time)
    tic
    for i = 1:3
        balls(i).XData = CamX(i,k);
        balls(i).YData = CamY(i,k);
        ests(i).XData = EstX(i,k);
        ests(i).YData = EstY(i,k);
        cons(i).XData = ConX(i,k);
        cons(i).YData = ConY(i,k);
        refs(i).XData = RefX(i,k);
        refs(i).YData = RefY(i,k);
    end
    title(sprintf('Time %03.2f s', time(k)))
    pause(0.02/playback_speed - min(0.02/playback_speed, toc))
end