%% settings
folder = '06';

fps = 25;

start_time = 0;
stop_time = 59.96;

trajectory_period = 20;
multiple_trajectories = false;
traj_colors = {[0.7,0.7,0.7]};
% traj_colors = {[0.65,0,0], [0.8,0.8,0], [0.24,0.65,1]};

ref_size = 250;
ref_colors = {[0.65,0,0], [0.8,0.8,0], [0.24,0.65,1]};

save_gif = true;

%% loading
load(sprintf('%s/RGBballs.mat', folder))
load('calibration.mat')

start_index = start_time*fps;
duration = round((stop_time - start_time)*fps + 1);
ref_x = zeros(3, duration);
ref_y = zeros(3, duration);
cam_x = zeros(3, duration);
cam_y = zeros(3, duration);

for k = 1:duration
    ind = k + start_index;
    [xR,yR] = inverse_transform(reference(ind,1), reference(ind,2), H_unrot, correction*1e3);
    [xG,yG] = inverse_transform(reference(ind,3), reference(ind,4), H_unrot, correction*1e3);
    [xB,yB] = inverse_transform(reference(ind,5), reference(ind,6), H_unrot, correction*1e3);
    ref_x(:,k) = [xR,xG,xB];
    ref_y(:,k) = [yR,yG,yB];
    [xR,yR] = inverse_transform(ballpos(ind,1), ballpos(ind,2), H_unrot, correction*1e3);
    [xG,yG] = inverse_transform(ballpos(ind,3), ballpos(ind,4), H_unrot, correction*1e3);
    [xB,yB] = inverse_transform(ballpos(ind,5), ballpos(ind,6), H_unrot, correction*1e3);
    cam_x(:,k) = [xR,xG,xB];
    cam_y(:,k) = [yR,yG,yB];
end

%% plot
figure(1)
clf
frame_plot = imshow(255*ones(480,480,3,'uint8'));
set(gcf,'Units','pixels')
set(gcf,'Position',[300,200,480,480])
set(gca,'Position',[0,0,1,1])
hold on
refs(1) = plot(ref_x(1,1:trajectory_period*fps), ref_y(1,1:trajectory_period*fps), '--', 'Color', traj_colors{1}, 'LineWidth', 4);
if multiple_trajectories    
    refs(2) = plot(ref_x(2,1:trajectory_period*fps), ref_y(2,1:trajectory_period*fps), '--', 'Color', traj_colors{2}, 'LineWidth', 4);
    refs(3) = plot(ref_x(3,1:trajectory_period*fps), ref_y(3,1:trajectory_period*fps), '--', 'Color', traj_colors{3}, 'LineWidth', 4);
end
if save_gif
    print(sprintf('%s/sprite_01.svg', folder), '-dsvg', '-r0')
end
ref_RK = scatter(ref_x(1,1),ref_y(1,1),ref_size*1.5,'x','LineWidth',4,'MarkerEdgeColor',[0,0,0]);
ref_R = scatter(ref_x(1,1),ref_y(1,1),ref_size,'x','LineWidth',2,'MarkerEdgeColor',ref_colors{1});
ref_GK = scatter(ref_x(2,1),ref_y(2,1),ref_size*1.5,'x','LineWidth',4,'MarkerEdgeColor',[0,0,0]);
ref_G = scatter(ref_x(2,1),ref_y(2,1),ref_size,'x','LineWidth',2,'MarkerEdgeColor',ref_colors{2});
ref_BK = scatter(ref_x(3,1),ref_y(3,1),ref_size*1.5,'x','LineWidth',4,'MarkerEdgeColor',[0,0,0]);
ref_B = scatter(ref_x(3,1),ref_y(3,1),ref_size,'x','LineWidth',2,'MarkerEdgeColor',ref_colors{3});
if ~save_gif
    ball_R = scatter(100,100,ref_size,'o','LineWidth',2,'MarkerEdgeColor',ref_colors{1});
    ball_G = scatter(100,100,ref_size,'o','LineWidth',2,'MarkerEdgeColor',ref_colors{2});
    ball_B = scatter(100,100,ref_size,'o','LineWidth',2,'MarkerEdgeColor',ref_colors{3});
else
    print(sprintf('%s/sprite_02.svg', folder), '-dsvg', '-r0')
    delete(refs)
end
for k = 1:duration
    if ~save_gif
        tic
    end
    ref_RK.XData = ref_x(1,k);
    ref_RK.YData = ref_y(1,k);
    ref_R.XData = ref_x(1,k);
    ref_R.YData = ref_y(1,k);
    ref_GK.XData = ref_x(2,k);
    ref_GK.YData = ref_y(2,k);
    ref_G.XData = ref_x(2,k);
    ref_G.YData = ref_y(2,k);
    ref_BK.XData = ref_x(3,k);
    ref_BK.YData = ref_y(3,k);
    ref_B.XData = ref_x(3,k);
    ref_B.YData = ref_y(3,k);
    if ~save_gif
        ball_R.XData = cam_x(1,k);
        ball_R.YData = cam_y(1,k);
        ball_G.XData = cam_x(2,k);
        ball_G.YData = cam_y(2,k);
        ball_B.XData = cam_x(3,k);
        ball_B.YData = cam_y(3,k);
        
        pause(0.04 - toc);
    else
        I = print('-RGBImage', '-r0');
        [im,map] = rgb2ind(I,256,'nodither');

        if k == 1
            gifmode = 'overwrite';
        else
            gifmode = 'append';
        end
        imwrite(im, map, sprintf('%s/anno.gif', folder), 'DelayTime', 1/fps, ...
            'WriteMode', gifmode, 'DisposalMethod', 'doNotSpecify');
        pause(0)
    end
end