%% identification
[t,xax,yax,rax] = triangle_plot_layout(1);
plot_triangle_position('identification/triangle/full_volume/04.mat',xax,yax,rax,25*30,25*40,'--',true,true);
plot_triangle_position('identification/triangle/along_edge/04.mat',xax,yax,rax,25*30,25*40,'--',false,true);
plot_triangle_position('identification/triangle/closest_point/04.mat',xax,yax,rax,25*30,25*40,'--',false,true);
xlim(rax, [0 10])
ylim(xax,[-21.5 10])
ylim(yax,[-20 40])
ylim(rax,[-2 2])
rax.YAxis.TickValues = [-pi/2, 0, pi/2];
rax.YAxis.TickLabels = {'$-\pi/2$', '$0$', '$\pi/2$'};
title(t, 'LQR performance for different models', 'Interpreter', 'latex')
legend(xax, 'Reference', 'Full volume', 'Only edges', 'Closest point', 'Location', 'SouthEast', 'Interpreter', 'latex')

%% LQR vs MPC
[t,xax,yax,rax] = triangle_plot_layout(2);
plot_triangle_position('triangle/05/triangle.mat',xax,yax,rax,25*30,25*60,'--',true);
plot_triangle_position('triangle/06/triangle.mat',xax,yax,rax,25*30,25*60,'--',false);
xlim(rax, [0 30])
ylim(rax,[0 2*pi])
rax.YAxis.TickValues = [0, pi, 2*pi];
rax.YAxis.TickLabels = {'$0$', '$\pi$', '$2\pi$'};
title(t, 'LQR vs MPC', 'Interpreter', 'latex')
legend(xax, 'Reference', 'LQR', 'MPC', 'Location', 'SouthEast', 'Interpreter', 'latex')
