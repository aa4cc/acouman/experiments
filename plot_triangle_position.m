function [xplot, yplot, rplot, xrefplot, yrefplot, rrefplot] = plot_triangle_position(filename, xax, yax, rax, start_sample, end_sample, linespec, plot_reference, zero_reference, select)
    if nargin < 10
        select = 1;
        if nargin < 9
            zero_reference = false;
        end
    end
    S = load(filename);
    
    if plot_reference
        if ~zero_reference
            for k = select            
                xrefplot(k) = plot(xax, S.tout(start_sample:end_sample) - S.tout(start_sample), S.reference(start_sample:end_sample, 3*k-2), 'LineWidth', 1.5);
                yrefplot(k) = plot(yax, S.tout(start_sample:end_sample) - S.tout(start_sample), S.reference(start_sample:end_sample, 3*k-1), 'LineWidth', 1.5);
                theta = S.reference(start_sample:end_sample, 3*k);
                theta = theta - round(theta(1)/(2*pi))*2*pi;
                rrefplot(k) = plot(rax, S.tout(start_sample:end_sample) - S.tout(start_sample), theta, 'LineWidth', 1.5);                
            end
        else
            n_samples = end_sample - start_sample + 1;
            xrefplot = plot(xax, S.tout(start_sample:end_sample) - S.tout(start_sample), zeros(n_samples,1), 'LineWidth', 1.5);
            yrefplot = plot(yax, S.tout(start_sample:end_sample) - S.tout(start_sample), zeros(n_samples,1), 'LineWidth', 1.5);
            rrefplot = plot(rax, S.tout(start_sample:end_sample) - S.tout(start_sample), zeros(n_samples,1), 'LineWidth', 1.5);
        end
    end
    
    for k = select
        x = S.tripos(start_sample:end_sample, 3*k-2);
        y = S.tripos(start_sample:end_sample, 3*k-1);
        theta = unwrap(S.tripos(start_sample:end_sample, 3*k));
        theta = theta - round(theta(1)/(2*pi))*2*pi;
        if zero_reference
            x = x - S.reference(start_sample:end_sample, 3*k-2);
            y = y - S.reference(start_sample:end_sample, 3*k-1);
            theta = theta - S.reference(start_sample:end_sample, 3*k);
            theta = theta - round(mean(theta)/(2*pi))*2*pi;
        end
        xplot(k) = plot(xax, S.tout(start_sample:end_sample) - S.tout(start_sample), x, linespec, 'LineWidth', 1.5);
        yplot(k) = plot(yax, S.tout(start_sample:end_sample) - S.tout(start_sample), y, linespec, 'LineWidth', 1.5);
        rplot(k) = plot(rax, S.tout(start_sample:end_sample) - S.tout(start_sample), theta, linespec, 'LineWidth', 1.5);
    end
end
