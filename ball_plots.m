%% infinity -- amplitude vs gradient
[t,xax,yax] = ball_plot_layout(1);
plot_ball_position('RGBballs/01/RGBballs.mat', xax, yax, 25*30, 25*60, '--', true);
plot_ball_position('floating_ball_gradient_control/01.mat', xax, yax, 25*30, 25*60, '--', false);
ylim(xax,[-45 45])
ylim(yax,[-45 45])
xlim(xax,[0 30])
xlim(yax,[0 30])
title(t, 'Tracking the `Infinity'' trajectory', 'Interpreter', 'latex')
legend(xax, 'Reference', 'Amplitude control', 'Gradient control', 'Location', 'SouthEast', 'Interpreter', 'latex')
pretty_plot(1.1)

%% double link -- amplitude vs gradient
[t,xax,yax] = ball_plot_layout(2);
plot_ball_position('RGBballs/05/RGBballs.mat', xax, yax, 25*30, 25*60, '--', true);
plot_ball_position('floating_ball_gradient_control/02.mat', xax, yax, 25*30, 25*60, '--', false);
ylim(xax,[-55 55])
ylim(yax,[-40 40])
xlim(xax,[0 30])
xlim(yax,[0 30])
title(t, 'Tracking the `Double link'' trajectory', 'Interpreter', 'latex')
legend(xax, 'Reference', 'Amplitude control', 'Gradient control', 'Location', 'SouthEast', 'Interpreter', 'latex')
pretty_plot(1.1)

%% infinity -- fastest tracking
[t,xax,yax] = ball_plot_layout(3);
plot_ball_position('RGBballs/03/RGBballs.mat', xax, yax, 25*20, 25*40, '--', true);
ylim(xax,[-45 45])
ylim(yax,[-45 45])
xlim(xax,[0 20])
xlim(yax,[0 20])
title(t, 'Tracking the `Infinity'' trajectory', 'Interpreter', 'latex')
legend(xax, 'Reference', 'Measured position', 'Location', 'SouthEast', 'Interpreter', 'latex')
pretty_plot(1.1)

%% trajectory switching
[t,xax,yax] = ball_plot_layout(4);
plot_ball_position('RGBballs/06/RGBballs.mat', xax, yax, 25*10, 25*50, '--', true);
ylim(xax,[-55 55])
ylim(yax,[-45 45])
xlim(xax,[0 40])
xlim(yax,[0 40])
title(t, 'Trajectory switching', 'Interpreter', 'latex')
legend(xax, 'Reference', 'Measured position', 'Location', 'NorthEast', 'Interpreter', 'latex')
pretty_plot(1.1)