function pretty_plot(rel_width)
    cf = gcf;
    cax = cf.Children;
    
    width = rel_width * 15.29;
    
    for k = 1:numel(cax)
        ca = cax(k);
        if isa(ca, 'matlab.graphics.axis.Axes')
            if ~isempty(ca.Legend)
                ca.Legend.Interpreter = 'latex';
            end
            if ~isempty(ca.Title)
                ca.Title.Interpreter = 'latex';
            end
            if ~isempty(ca.XLabel)
                ca.XLabel.Interpreter = 'latex';
            end
            if ~isempty(ca.YLabel)
                ca.YLabel.Interpreter = 'latex';
            end
            ca.XAxis.TickLabelInterpreter = 'latex';
            ca.YAxis.TickLabelInterpreter = 'latex';
        end
    end
    
    cf.Units = 'centimeters';
    pos = cf.Position;
    height = pos(4) / pos(3) * width;
    cf.Position = [pos(1:2), width, height];
end
    