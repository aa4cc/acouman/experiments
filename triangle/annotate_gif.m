%% settings
folder = '06';

fps = 25;

start_time = 30;
stop_time = 60;

trajectory_period = 30;
multiple_trajectories = false;
traj_colors = {[0.7,0.7,0.7]};
% traj_colors = {[0.65,0,0], [0.24,0.65,1]};

triangle_width = 14;
triangle_height = 18;

ref_size = 250;
ref_colors = {[0.65,0,0], [0.24,0.65,1]};

save_gif = true;

%% loading
load(sprintf('%s/triangle.mat', folder))
load('calibration.mat')

num_triangles = size(tripos, 2) / 3;

A0 = [-triangle_height/3; triangle_width/2];
B0 = [-triangle_height/3; -triangle_width/2];
C0 = [2*triangle_height/3; 0];

start_index = start_time*fps;
duration = round((stop_time - start_time)*fps + 1);
ref_x = zeros(3, duration, num_triangles);
ref_y = zeros(3, duration, num_triangles);
cam_x = zeros(3, duration, num_triangles);
cam_y = zeros(3, duration, num_triangles);

for k = 1:duration
    ind = k + start_index;
    for j = 0:num_triangles-1
        x = reference(ind, 3*j+1);
        y = reference(ind, 3*j+2);
        r = reference(ind, 3*j+3);
        rot_mat = [cos(r), -sin(r); sin(r), cos(r)];
        A = rot_mat*A0 + [x;y];
        B = rot_mat*B0 + [x;y];
        C = rot_mat*C0 + [x;y];
        [xA,yA] = inverse_transform(A(1), A(2), H_unrot, correction*1e3);
        [xB,yB] = inverse_transform(B(1), B(2), H_unrot, correction*1e3);
        [xC,yC] = inverse_transform(C(1), C(2), H_unrot, correction*1e3);
        ref_x(:, k, j+1) = [xA;xB;xC];
        ref_y(:, k, j+1) = [yA;yB;yC];
        
        x = tripos(ind, 3*j+1);
        y = tripos(ind, 3*j+2);
        r = tripos(ind, 3*j+3);
        rot_mat = [cos(r), -sin(r); sin(r), cos(r)];
        A = rot_mat*A0 + [x;y];
        B = rot_mat*B0 + [x;y];
        C = rot_mat*C0 + [x;y];
        [xA,yA] = inverse_transform(A(1), A(2), H_unrot, correction*1e3);
        [xB,yB] = inverse_transform(B(1), B(2), H_unrot, correction*1e3);
        [xC,yC] = inverse_transform(C(1), C(2), H_unrot, correction*1e3);
        cam_x(:, k, j+1) = [xA;xB;xC];
        cam_y(:, k, j+1) = [yA;yB;yC];
    end
end

%% plot
figure(1)
clf
frame_plot = imshow(255*ones(480,480,3,'uint8'));
set(gcf,'Units','pixels')
set(gcf,'Position',[300,200,480,480])
set(gca,'Position',[0,0,1,1])
hold on
traj_plot(1) = plot(mean(ref_x(:,1:trajectory_period*fps,1), 1), mean(ref_y(:,1:trajectory_period*fps,1), 1), '--', 'Color', traj_colors{1}, 'LineWidth', 4);
if multiple_trajectories    
    traj_plot(2) = plot(mean(ref_x(:,1:trajectory_period*fps,1), 2), mean(ref_y(:,1:trajectory_period*fps,1), 2), '--', 'Color', traj_colors{2}, 'LineWidth', 4);
end
if save_gif
    print(sprintf('%s/sprite_01.svg', folder), '-dsvg', '-r0')
end
for k = 1:num_triangles
    ref_bck(k) = closed_plot(ref_x(:, 1, k), ref_y(:, 1, k), 'k-', 'LineWidth', 4);
    ref_plot(k) = closed_plot(ref_x(:, 1, k), ref_y(:, 1, k), '--', 'Color', ref_colors{k}, 'LineWidth', 2);
end
if ~save_gif
    for k = 1:num_triangles
        tri_plot(k) = closed_plot(cam_x(:, 1, k), cam_y(:, 1, k), 'k-', 'LineWidth', 4);
    end
else
    print(sprintf('%s/sprite_02.svg', folder), '-dsvg', '-r0')
    delete(traj_plot)
end
for k = 1:duration
    if ~save_gif
        tic
    end
    for j = 1:num_triangles        
        edit_plot(ref_bck(j), ref_x(:, k, j), ref_y(:, k, j));
        edit_plot(ref_plot(j), ref_x(:, k, j), ref_y(:, k, j));
        if ~save_gif
            edit_plot(tri_plot(j), cam_x(:, k, j), cam_y(:, k, j));
            pause(0.04 - toc);
        end
    end
    if save_gif
        I = print('-RGBImage', '-r0');
        [im,map] = rgb2ind(I,256,'nodither');

        if k == 1
            gifmode = 'overwrite';
        else
            gifmode = 'append';
        end
        imwrite(im, map, sprintf('%s/anno.gif', folder), 'DelayTime', 1/fps, ...
            'WriteMode', gifmode, 'DisposalMethod', 'doNotSpecify');
        pause(0)
    end
end

%% Support function
function plt = closed_plot(x, y, varargin)
    plt = plot([x; x(1)], [y; y(1)], varargin{:});
end

function edit_plot(plt, x, y)
    plt.XData = [x; x(1)];
    plt.YData = [y; y(1)];
end