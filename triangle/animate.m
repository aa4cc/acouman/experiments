%% parameters
folder = '09';

point_size = 150;
area_size = 70;

width = 14;
height = 18;

colors = {[1 0 0], [0 1 0], [1 1 0], [0 0 1]};

%% loading
A0 = [-height/3; width/2];
B0 = [-height/3; -width/2];
C0 = [2*height/3; 0];

load(sprintf('%s/triangle.mat', folder));

num_obj = size(tripos, 2) / 3;

conX = controller(:,1:num_obj) * 1e3;
conY = controller(:,(num_obj+1):(2*num_obj)) * 1e3;

objX = tripos(:,1:3:end);
objY = tripos(:,2:3:end);
objRot = tripos(:,3:3:end);

refX = reference(:,1:3:end);
refY = reference(:,2:3:end);
refRot = reference(:,3:3:end);

%% plotting
figure(1)
clf
hold on
for i = 1:num_obj
    tri_plot(i) = patch([A0(1),B0(1),C0(1)], [A0(2),B0(2),C0(2)], colors{i});
    ref_plot(i) = patch([A0(1),B0(1),C0(1)], [A0(2),B0(2),C0(2)], colors{i}, 'FaceColor', 'none', 'EdgeColor', colors{i}, 'LineStyle', '--', 'LineWidth', 2);
end
point = scatter([0,1,2],[0,1,2],point_size,'ro','filled');
xlim([-area_size,area_size])
ylim([-area_size,area_size])
for k = 1:numel(tout)
    for i = 1:num_obj
        rot_mat = [cos(objRot(k,i)), -sin(objRot(k,i)); sin(objRot(k,i)), cos(objRot(k,i))];
        tri_points = rot_mat * [A0, B0, C0] + [objX(k,i); objY(k,i)];
        tri_plot(i).Vertices = tri_points';
        
        rot_mat = [cos(refRot(k,i)), -sin(refRot(k,i)); sin(refRot(k,i)), cos(refRot(k,i))];
        tri_points = rot_mat * [A0, B0, C0] + [refX(k,i); refY(k,i)];
        ref_plot(i).Vertices = tri_points';
    end
    
    
    point.XData = conX(k, :);
    point.YData = conY(k, :);
    
    title(sprintf('Time %.2f', tout(k)))
    pause(1/25)
end