%% settings
folder = '09';

% outline_colors = {[1,0,0], [0.24,0.65,1]};
outline_colors = {[1,0,0], [0,1,0], [1,1,0], [0.24,0.65,1]};

triangle_width = 14;
triangle_height = 18;

width = 14;
height = 18;

% fill_colors = {[1,0,0], [0.12,0.32,1]};
fill_colors = {[1,0,0], [0,1,0], [1,1,0], [0.12,0.32,1]};
opacity = 0.33;

preview = false;

%% loading
load(sprintf('%s/formation.mat', folder))
load('calibration.mat')

num_triangles = size(goal, 1);

A0 = [-height/3; width/2];
B0 = [-height/3; -width/2];
C0 = [2*height/3; 0];

home_vertices = zeros(3,2,num_triangles);
goal_vertices = zeros(3,2,num_triangles);
for k = 1:num_triangles
    x = home(k, 1);
    y = home(k, 2);
    r = home(k, 3);
    rot_mat = [cos(r), -sin(r); sin(r), cos(r)];
    A = rot_mat*A0 + [x;y];
    B = rot_mat*B0 + [x;y];
    C = rot_mat*C0 + [x;y];
    [xA,yA] = inverse_transform(A(1), A(2), H_unrot, correction*1e3);
    [xB,yB] = inverse_transform(B(1), B(2), H_unrot, correction*1e3);
    [xC,yC] = inverse_transform(C(1), C(2), H_unrot, correction*1e3);
    home_vertices(:,:,k) = [xA,yA;xB,yB;xC,yC];
    
    x = goal(k, 1);
    y = goal(k, 2);
    r = goal(k, 3);
    rot_mat = [cos(r), -sin(r); sin(r), cos(r)];
    A = rot_mat*A0 + [x;y];
    B = rot_mat*B0 + [x;y];
    C = rot_mat*C0 + [x;y];
    [xA,yA] = inverse_transform(A(1), A(2), H_unrot, correction*1e3);
    [xB,yB] = inverse_transform(B(1), B(2), H_unrot, correction*1e3);
    [xC,yC] = inverse_transform(C(1), C(2), H_unrot, correction*1e3);
    goal_vertices(:,:,k) = [xA,yA;xB,yB;xC,yC];
end

f = [1,2,3];

%% plotting
figure
if preview
    hold on
end
for k = 1:num_triangles
    if ~preview
        clf
    end
    patch('Faces', f, 'Vertices', home_vertices(:,:,k), 'FaceColor', 'none', 'EdgeColor', outline_colors{k}, 'LineWidth', 2, 'LineStyle', '--')
    xlim([0 480])
    ylim([0 480])
    set(gca, 'YDir', 'reverse')
    set(gcf,'Units','pixels')
    set(gcf,'Position',[300,200,480,480])
    set(gca,'Position',[0,0,1,1])
    if ~preview
        print(sprintf('%s/home_%d.svg', folder, k), '-dsvg', '-r0')
    end
    
    if ~preview
        clf
    end
    patch('Faces', f, 'Vertices', goal_vertices(:,:,k), 'FaceColor', fill_colors{k}, 'EdgeColor', outline_colors{k}, 'LineWidth', 0.75, 'FaceAlpha', opacity)
    if ~preview
        xlim([0 480])
        ylim([0 480])
        set(gca, 'YDir', 'reverse')
        set(gcf,'Units','pixels')
        set(gcf,'Position',[300,200,480,480])
        set(gca,'Position',[0,0,1,1])
        print(sprintf('%s/goal_%d.svg', folder, k), '-dsvg', '-r0')
    end
end