function [x_c, y_c] = inverse_transform(x_e, y_e, H, c)
    A = H \ [x_e; y_e; c];
    x_c = A(1) / A(3);
    y_c = A(2) / A(3);
end

