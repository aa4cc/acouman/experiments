function [A,B,C,D,K] = second_order_integrator(G, b, ~)
    A = [-b, 0; 1 0];
    B = [G; 0];
    C = [0 1];
    D = 0;
    K = [1; 0];
end