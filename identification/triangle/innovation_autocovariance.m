%% parameters
name = 'full_volume/08';

start_index = 550;
length = 25;

delay = 2;

%% loading
load(name)

innovation = tripos(start_index+delay:start_index+length+delay,:) - kalman(start_index:start_index+length,:);

%% calculating autocovariance
lag = -length:length;
X = zeros(2*length+1, 3);
for k = 1:3
    X(:, k) = xcov(innovation(:, k));
    X(:, k) = X(:, k) / max(X(:, k));
end

%% plotting
figure(1)
clf
stem(lag, X(:,1),'MarkerSize',4)
hold on
stem(lag, X(:,2),'--','MarkerSize',4)
stem(lag, X(:,3),':','MarkerSize',4,'Color',[0.4940,0.1840,0.5560])
grid on
legend 'x-coordinate' 'y-coordinate' '$\theta$'
title 'Normalized innovation autocovariance'
xlabel 'Lag [samples]'
ylabel 'Normalized autocovariance'
xlim([-25 25])
pretty_plot(0.85)