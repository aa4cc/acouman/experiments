function [A_t,B_t,A_r,B_r] = identify(name, plot_verification)

if nargin < 2
    plot_verification = false;
end

%% loading
S = load(name);

x = double(S.tripos(:,1));
y = double(S.tripos(:,2));
rot = double(unwrap(S.tripos(:,3)));

F_x = double(S.force(:,1));
F_y = double(S.force(:,2));
T = double(S.force(:,3));

Ts = mean(diff(S.tout));

%% Estimate derivative
d_b = [1, 0, -1]; % unfiltered derivative
d_a = [2*Ts, 0]; 
v_x = filter(d_b, d_a, x);
v_y = filter(d_b, d_a, y);
omega = filter(d_b, d_a, rot);

%% identification
sys = idgrey('second_order_integrator', {'Gain', 1; 'Drag', 1}, 'c', 'InputDelay', 0.08);

xdata = iddata(x, F_x, Ts);
ydata = iddata(y, F_y, Ts);
rdata = iddata(rot, T, Ts);

xmodel = greyest(xdata, sys);
ymodel = greyest(ydata, sys);
rmodel = greyest(rdata, sys);

%% Get model parameters
A_t = (xmodel.A + ymodel.A) / 2;
B_t = (xmodel.B + ymodel.B) / 2;
trans_model = ss(A_t, B_t, [0 1], 0);

A_r = rmodel.A;
B_r = rmodel.B;
rot_model = ss(A_r, B_r, [0 1], 0);

%% verification
if plot_verification
    x_ver = lsim(trans_model, F_x(5:end), S.tout(5:end), [v_x(5); x(5)]);
    y_ver = lsim(trans_model, F_y(5:end), S.tout(5:end), [v_y(5); y(5)]);
    rot_ver = lsim(rot_model, T(5:end), S.tout(5:end), [omega(5); rot(5)]);

    figure
    subplot(3,1,1)
    plot(S.tout, x, ':')
    hold on
    plot(S.tout(5:end), x_ver, '--')
    legend('Measured', 'Model predicted')
    subplot(3,1,2)
    plot(S.tout, y, ':')
    hold on
    plot(S.tout(5:end), y_ver, '--')
    legend('Measured', 'Model predicted')
    subplot(3,1,3)
    plot(S.tout, rot, ':')
    hold on
    plot(S.tout(5:end), rot_ver, '--')
    legend('Measured', 'Model predicted')
end
