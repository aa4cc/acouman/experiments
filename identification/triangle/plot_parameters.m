%% parameters
folder = 'full_volume';

experiments = {'01','02','03','04','05','06','07','08'};

%% identification
num_exp = numel(experiments);
d_t = zeros(num_exp,1);
d_r = zeros(num_exp,1);
g_t = zeros(num_exp,1);
g_r = zeros(num_exp,1);

for k = 1:num_exp
    [A_t,B_t,A_r,B_r] = identify([folder,'/',experiments{k}]);
    d_t(k) = - A_t(1,1);
    d_r(k) = - A_r(1,1);
    g_t(k) = B_t(1);
    g_r(k) = B_r(1);
end

%% plotting
figure(1)
clf
t = tiledlayout(2,2);
dt_ax = nexttile;
hold on
grid on
m = plot([1,num_exp], mean(d_t(4:end))*[1,1], 'b--', 'LineWidth', 1.25);
id = plot(1:num_exp, d_t, 'b*');
grid on
set(dt_ax, 'YMinorGrid', 'on')
set(dt_ax, 'XTick', 1:num_exp)
set(dt_ax, 'XTickLabel', {})
xlim([0.5,num_exp+0.5])
ylabel('$d_t$', 'Interpreter', 'latex')
legend([id,m],'Identified', 'Mean', 'Interpreter', 'latex', 'Location', 'NorthEast')
title(t, 'Identified parameters for `Full volume'' model', 'Interpreter', 'latex')

dr_ax = nexttile;
hold on
grid on
plot([1,num_exp], mean(d_r(4:end))*[1,1], 'b--', 'LineWidth', 1.25);
plot(1:num_exp, d_r, 'b*');
grid on
set(dr_ax, 'YMinorGrid', 'on')
set(dr_ax, 'XTick', 1:num_exp)
set(dr_ax, 'XTickLabel', {})
xlim([0.5,num_exp+0.5])
ylabel('$d_r$', 'Interpreter', 'latex')

gt_ax = nexttile;
hold on
grid on
plot([1,num_exp], mean(g_t(4:end))*[1,1], 'b--', 'LineWidth', 1.25);
plot(1:num_exp, g_t, 'b*');
grid on
set(gt_ax, 'YMinorGrid', 'on')
set(gt_ax, 'XTick', 1:num_exp)
xlim([0.5,num_exp+0.5])
ylabel('$g_t$', 'Interpreter', 'latex')
xlabel('Iteration', 'Interpreter', 'latex')
linkaxes([dt_ax,gt_ax],'x');

gr_ax = nexttile;
hold on
grid on
plot([1,num_exp], mean(g_r(4:end))*[1,1], 'b--', 'LineWidth', 1.25);
plot(1:num_exp, g_r, 'b*');
grid on
set(gr_ax, 'YMinorGrid', 'on')
set(gr_ax, 'XTick', 1:num_exp)
xlim([0.5,num_exp+0.5])
ylabel('$g_r$', 'Interpreter', 'latex')
xlabel('Iteration', 'Interpreter', 'latex')
linkaxes([dr_ax,gr_ax],'x');

t.TileSpacing = 'compact';