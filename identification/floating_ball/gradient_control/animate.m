%% parameters
area_size = 70;
ball_size = 500;
est_size = 250;
folder = '05';

%% loading
load(sprintf('%s/ballpos.mat', folder))
load(sprintf('%s/controller.mat', folder))
load(sprintf('%s/kalman.mat', folder))
load(sprintf('%s/reference.mat', folder))

time = ballpos(1,:);
x_meas = ballpos(2,:);
y_meas = ballpos(3,:);
x_est = kalman(2,:);
y_est = kalman(3,:);
x_ctrl = controller(2,:) * 1e3;
y_ctrl = controller(3,:) * 1e3;
x_ref = ref(2,:);
y_ref = ref(3,:);
F_x = - controller(4,:) / 5e7;
F_y = - controller(5,:) / 5e7;

%% plotting animation
figure(1)
clf
ball = scatter(0, 0, ball_size, 'ro', 'filled');
hold on
est = scatter(0,0, est_size, 'kx', 'LineWidth', 1.5);
fvector = plot([0,1],[0,0],'-bd','LineWidth',1.5);
reference = scatter(0,0, est_size, 'ko', 'LineWidth', 1.5);
title 'Time 0.00'
xlim([-area_size, area_size])
ylim([-area_size, area_size])

for k = 1:numel(time)-4
    tic
    ball.XData = x_meas(k+4);
    ball.YData = y_meas(k+4);
    est.XData = x_est(k);
    est.YData = y_est(k);
    fvector.XData = x_ctrl(k) + [0, F_x(k)];
    fvector.YData = y_ctrl(k) + [0, F_y(k)];
    reference.XData = x_ref(k);
    reference.YData = y_ref(k);
    pause(max(1/50 - toc, 0))
    title(sprintf('Time %.2f s', time(k)))
end
