function [A,B,C,D,K] = state_matrices(g, d, ~)
    A = [-d, 0; 1 0];
    B = [g; 0];
    C = [0 1];
    D = 0;
    K = [1; 0];
end