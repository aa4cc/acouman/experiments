function [d, g] = identify(folder)
%% Load data
load(sprintf('%s/ballpos.mat', folder));
load(sprintf('%s/controller.mat', folder));

x = ballpos(2,:);
y = ballpos(3,:);
Gx = controller(4,:) / 1e6;
Gy = controller(5,:) / 1e6;

Ts = 0.02;

%% Create datasets
start_sample = 10;
xdata = iddata(x(start_sample:end)', Gx(start_sample:end)', Ts);
ydata = iddata(y(start_sample:end)', Gy(start_sample:end)', Ts);

%% Estimate models
sys = idgrey('state_matrices', {'Gain', 1; 'Drag', 1}, 'c', 'InputDelay', 0.08);
ss1 = greyest(xdata, sys);
ss2 = greyest(ydata, sys);

%% Continuous-time model
Ac = (ss1.A + ss2.A) / 2;
Bc = (ss1.B + ss2.B) / 2;
Cc = [0, 1];
Dc = 0;
Kc = [1;0];
Qc = (ss1.NoiseVariance + ss2.NoiseVariance) / 2 * Kc;
Gc = [1; 0];

d = -Ac(1,1);
g = Bc(1);

%% Discretization
Ts = 0.02;
ABd = expm([Ac, Bc; zeros(1,3)] * Ts);
A_g = ABd(1:2, 1:2);
B_g = ABd(1:2, 3);
C_g = Cc;
D_g = Dc;

syms tau
Q_g = int(expm(Ac*(Ts-tau))*Gc*Gc'*expm(Ac*(Ts-tau))',tau,0,Ts)*Qc;
Q_g = double(Q_g);

%% Save
save(sprintf('%s/state_space_grad.mat', folder),'-regexp','[ABCDQ]_g')
end