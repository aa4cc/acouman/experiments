function [d, g] = identify(folder)
%% Load data
S1 = load(sprintf('%s/ballpos.mat',folder));
S2 = load(sprintf('%s/force.mat',folder));

% time = S1.ballpos(1,:);
x = S1.ballpos(2,:);
y = S1.ballpos(3,:);
Px = S2.force(2,:);
Py = S2.force(3,:);

Ts = 0.02;

%% Create datasets
start_sample = 10;
xdata = iddata(x(start_sample:end)', Px(start_sample:end)', Ts);
ydata = iddata(y(start_sample:end)', Py(start_sample:end)', Ts);

%% Estimate models
sys = idgrey('state_matrices', {'Gain', 1; 'Drag', 1}, 'c', 'InputDelay', 0.08);
ss1 = greyest(xdata, sys);
ss2 = greyest(ydata, sys);

%% Continuous-time model
Ac = (ss1.A + ss2.A) / 2;
Bc = (ss1.B + ss2.B) / 2;
Cc = [0, 1];
Dc = 0;
Kc = [1;0];
Qc = (ss1.NoiseVariance + ss2.NoiseVariance) / 2 * Kc;
Gc = [1; 0];

d = -Ac(1,1);
g = Bc(1);

%% Discretization
Ts = 0.02;
ABd = expm([Ac, Bc; zeros(1,3)] * Ts);
A_p = ABd(1:2, 1:2);
B_p = ABd(1:2, 3);
C_p = Cc;
D_p = Dc;

syms tau
Q_p = int(expm(Ac*(Ts-tau))*Gc*Gc'*expm(Ac*(Ts-tau))',tau,0,Ts)*Qc;
Q_p = double(Q_p);

%% Save
save(sprintf('%s/state_space_press.mat',folder),'-regexp','[ABCDQ]_p')