%% specify experiment folders
folders = {'01','02','03','04','05','06'};

%% identify parameters
num_exp = numel(folders);
C_drag = zeros(num_exp, 1);
C_gain = zeros(num_exp, 1);

for k = 1:num_exp
    [d,g] = identify(folders{k});
    C_drag(k) = d;
    C_gain(k) = g;
end

%% plot results
figure
t = tiledlayout(2,1);
ax1 = nexttile;
m = plot([1,num_exp], mean(C_drag)*[1,1], 'b--', 'LineWidth', 1.25);
hold on
id = plot(1:num_exp, C_drag, 'b*');
grid on
set(ax1, 'YMinorGrid', 'on')
set(ax1, 'XTick', 1:num_exp)
set(ax1, 'XTickLabel', {})
xlim([0.5,num_exp+0.5])
ylim([1.5,3.75])
ylabel(ax1, '$d_p$', 'Interpreter', 'latex')
title(t, 'Amplitude fitting model', 'Interpreter', 'latex')
ax1.YAxis.TickLabelInterpreter = 'latex';
legend([id, m], 'Identified', 'Mean')

ax2 = nexttile;
plot([1,num_exp], mean(C_gain)*[1,1], 'b--', 'LineWidth', 1.25);
hold on
plot(1:num_exp, C_gain, 'b*')
grid on
set(ax2, 'YMinorGrid', 'on')
set(ax2, 'XTick', 1:num_exp)
xlim([0.5,num_exp+0.5])
ylim([0,250])
xlabel(ax2, 'Iteration', 'Interpreter', 'latex')
ylabel(ax2, '$g_p$', 'Interpreter', 'latex')
ax2.YAxis.TickLabelInterpreter = 'latex';
ax2.XAxis.TickLabelInterpreter = 'latex';
linkaxes([ax1,ax2],'x');
t.TileSpacing = 'compact';
pretty_plot(0.55)