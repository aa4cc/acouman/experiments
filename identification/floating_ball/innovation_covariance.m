%% parameters
folder = 'gradient_control/11';

start_index = 150;
length = 25;

delay = 2;

%% loading
load(sprintf('%s/ballpos.mat', folder))
load(sprintf('%s/kalman.mat', folder))

innovation = ballpos([1,2], start_index+delay:start_index+length+delay) - kalman([1,2], start_index:start_index+length);

%% calculating autocovariance
lag = -length:length;
X = zeros(2*length+1, 2);
for k = 1:2
    X(:, k) = xcov(innovation(k, :));
    X(:, k) = X(:, k) / max(X(:, k));
end

%% plotting
figure(1)
clf
stem(lag, X(:,1),'MarkerSize',4)
hold on
stem(lag, X(:,2),'--','MarkerSize',4)
grid minor
legend 'x-coordinate' 'y-coordinate'
title 'Normalized innovation autocovariance'
xlabel 'Lag [samples]'
ylabel 'Normalized autocovariance'
pretty_plot(0.55)