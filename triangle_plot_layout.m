function [t,xax,yax,rax] = triangle_plot_layout(n)
    if nargin < 1
        n = 1;
    end
    figure(n)
    clf
    t = tiledlayout(3,1);
    xax = nexttile;
    hold on
    grid on
    yax = nexttile;
    hold on
    grid on
    rax = nexttile;
    hold on
    grid on
    ylabel(xax, '$x$ [mm]', 'Interpreter', 'latex')
    xlabel(rax, '$t$ [s]', 'Interpreter', 'latex')
    ylabel(yax, '$y$ [mm]', 'Interpreter', 'latex')
    ylabel(rax, '$\theta$ [rad]', 'Interpreter', 'latex')
    xax.YAxis.TickLabelInterpreter = 'latex';
    xax.XAxis.TickLabels = {};
    yax.YAxis.TickLabelInterpreter = 'latex';
    yax.XAxis.TickLabels = {};
    rax.YAxis.TickLabelInterpreter = 'latex';
    rax.XAxis.TickLabelInterpreter = 'latex';
    linkaxes([xax,yax,rax],'x');
    t.TileSpacing = 'compact';
end
