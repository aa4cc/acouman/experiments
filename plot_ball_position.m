function [xplot, yplot, xrefplot, yrefplot] = plot_ball_position(filename, xax, yax, start_sample, end_sample, linespec, plot_reference, select)
    if nargin < 8
        select = 1;
    end
    S = load(filename);
    
    if plot_reference
        for k = select
            xrefplot(k) = plot(xax, S.tout(start_sample:end_sample) - S.tout(start_sample), S.reference(start_sample:end_sample, 2*k-1), 'LineWidth', 1.5);
            yrefplot(k) = plot(yax, S.tout(start_sample:end_sample) - S.tout(start_sample), S.reference(start_sample:end_sample, 2*k), 'LineWidth', 1.5);
        end
    end
    
    for k = select
        xplot(k) = plot(xax, S.tout(start_sample:end_sample) - S.tout(start_sample), S.ballpos(start_sample:end_sample, 2*k-1), linespec, 'LineWidth', 1.5);
        yplot(k) = plot(yax, S.tout(start_sample:end_sample) - S.tout(start_sample), S.ballpos(start_sample:end_sample, 2*k), linespec, 'LineWidth', 1.5);
    end
end
